/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ChangeObjectLabelRequest } from '../models/ChangeObjectLabelRequest.ts';
import type { ImportObjectRequest } from '../models/ImportObjectRequest.ts';
import type { ObjectArchivesResponse } from '../models/ObjectArchivesResponse.ts';
import type { TaskStateResponse } from '../models/TaskStateResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class ObjectRepositoryService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list3(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/actions/sync',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/actions/sync',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/actions/sync',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete3(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/actions/sync',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead3(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/actions/sync',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list31(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/archives',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel1(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/archives',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject1(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/archives',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete31(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/archives',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead31(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/archives',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list32(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/archives/{archiveId}/actions/sync',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel2(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/archives/{archiveId}/actions/sync',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject2(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/archives/{archiveId}/actions/sync',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete32(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/archives/{archiveId}/actions/sync',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead32(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/archives/{archiveId}/actions/sync',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list33(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/archives/{archiveId}/objects',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel3(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/archives/{archiveId}/objects',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject3(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/archives/{archiveId}/objects',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete33(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/archives/{archiveId}/objects',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead33(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/archives/{archiveId}/objects',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list34(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel4(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject4(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete34(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead34(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list35(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/label',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel5(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/label',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject5(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/label',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete35(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/label',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead35(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/label',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list36(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/resources/{resourceId}/url',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel6(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/resources/{resourceId}/url',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject6(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/resources/{resourceId}/url',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete36(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/resources/{resourceId}/url',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead36(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/archives/{archiveId}/objects/{objectId}/resources/{resourceId}/url',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

    /**
     * List all available object-archives.
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public list37(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/object-repository/tasks/{taskId}',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public putChangeLabel7(
        archiveId: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/object-repository/tasks/{taskId}',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject7(
        archiveId: string,
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/object-repository/tasks/{taskId}',
            path: {
                'archiveId': archiveId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete37(
        archiveId: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/object-repository/tasks/{taskId}',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
            },
        });
    }

    /**
     * @param archiveId
     * @param objectId
     * @param resourceId
     * @returns binary
     * @throws ApiError
     */
    public resolveHead37(
        archiveId: string,
        objectId: string,
        resourceId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/object-repository/tasks/{taskId}',
            path: {
                'archiveId': archiveId,
                'objectId': objectId,
                'resourceId': resourceId,
            },
        });
    }

}
