/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { UploadResponse } from '../models/UploadResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class UploadService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param requestBody
     * @returns UploadResponse
     * @throws ApiError
     */
    public upload(
        requestBody?: any,
    ): CancelablePromise<UploadResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/upload',
            body: requestBody,
        });
    }

}
