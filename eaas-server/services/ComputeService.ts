/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ComputeRequest } from '../models/ComputeRequest.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class ComputeService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param sessionId
     * @returns binary
     * @throws ApiError
     */
    public state(
        sessionId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/compute',
            path: {
                'sessionId': sessionId,
            },
        });
    }

    /**
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create(
        requestBody?: ComputeRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/compute',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param sessionId
     * @returns binary
     * @throws ApiError
     */
    public state1(
        sessionId: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/compute/{sessionId}',
            path: {
                'sessionId': sessionId,
            },
        });
    }

    /**
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create1(
        requestBody?: ComputeRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/compute/{sessionId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
