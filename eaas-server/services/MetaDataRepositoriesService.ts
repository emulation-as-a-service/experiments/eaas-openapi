/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CompletionStageOfResponse } from '../models/CompletionStageOfResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class MetaDataRepositoriesService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param name
     * @returns CompletionStageOfResponse
     * @throws ApiError
     */
    public list2(
        name: string,
    ): CancelablePromise<CompletionStageOfResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/metadata-repositories/{name}/item-identifiers',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public insert(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/metadata-repositories/{name}/item-identifiers',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public supported(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/metadata-repositories/{name}/item-identifiers',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns CompletionStageOfResponse
     * @throws ApiError
     */
    public list21(
        name: string,
    ): CancelablePromise<CompletionStageOfResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/metadata-repositories/{name}/items',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public insert1(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/metadata-repositories/{name}/items',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public supported1(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/metadata-repositories/{name}/items',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns CompletionStageOfResponse
     * @throws ApiError
     */
    public list22(
        name: string,
    ): CancelablePromise<CompletionStageOfResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/metadata-repositories/{name}/sets',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public insert2(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/metadata-repositories/{name}/sets',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public supported2(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/metadata-repositories/{name}/sets',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns CompletionStageOfResponse
     * @throws ApiError
     */
    public list23(
        name: string,
    ): CancelablePromise<CompletionStageOfResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/metadata-repositories/{name}/sets/{setspec}',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public insert3(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/metadata-repositories/{name}/sets/{setspec}',
            path: {
                'name': name,
            },
        });
    }

    /**
     * @param name
     * @returns binary
     * @throws ApiError
     */
    public supported3(
        name: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/metadata-repositories/{name}/sets/{setspec}',
            path: {
                'name': name,
            },
        });
    }

}
