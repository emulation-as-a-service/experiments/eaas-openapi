/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EnvironmentCreateRequest } from '../models/EnvironmentCreateRequest.ts';
import type { EnvironmentDeleteRequest } from '../models/EnvironmentDeleteRequest.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EnvironmentRepositoryService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/actions/create-image',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create2(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/actions/create-image',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete2(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/actions/create-image',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead2(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/actions/create-image',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/actions/create-image',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb1(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/actions/delete-image',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create21(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/actions/delete-image',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete21(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/actions/delete-image',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead21(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/actions/delete-image',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set1(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/actions/delete-image',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb2(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/actions/import-image',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create22(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/actions/import-image',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete22(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/actions/import-image',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead22(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/actions/import-image',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set2(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/actions/import-image',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb3(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/actions/prepare',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create23(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/actions/prepare',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete23(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/actions/prepare',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead23(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/actions/prepare',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set3(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/actions/prepare',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb4(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/actions/replicate-image',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create24(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/actions/replicate-image',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete24(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/actions/replicate-image',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead24(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/actions/replicate-image',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set4(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/actions/replicate-image',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb5(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/actions/sync',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create25(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/actions/sync',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete25(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/actions/sync',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead25(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/actions/sync',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set5(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/actions/sync',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb6(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/db-migration',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create26(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/db-migration',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete26(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/db-migration',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead26(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/db-migration',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set6(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/db-migration',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb7(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/default-environments',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create27(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/default-environments',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete27(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/default-environments',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead27(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/default-environments',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set7(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/default-environments',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb8(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/default-environments/{osId}',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create28(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/default-environments/{osId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete28(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/default-environments/{osId}',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead28(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/default-environments/{osId}',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set8(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/default-environments/{osId}',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb9(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/environments',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create29(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/environments',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete29(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/environments',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead29(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/environments',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set9(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/environments',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb10(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/environments/{envId}',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create210(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/environments/{envId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete210(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/environments/{envId}',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead210(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/environments/{envId}',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set10(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/environments/{envId}',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb11(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/environments/{envId}/export',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create211(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/environments/{envId}/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete211(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/environments/{envId}/export',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead211(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/environments/{envId}/export',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set11(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/environments/{envId}/export',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb12(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/environments/{envId}/object-deps',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create212(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/environments/{envId}/object-deps',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete212(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/environments/{envId}/object-deps',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead212(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/environments/{envId}/object-deps',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set12(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/environments/{envId}/object-deps',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb13(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/environments/{envId}/revisions',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create213(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/environments/{envId}/revisions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete213(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/environments/{envId}/revisions',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead213(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/environments/{envId}/revisions',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set13(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/environments/{envId}/revisions',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb14(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/environments/{envId}/revisions/{revId}',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create214(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/environments/{envId}/revisions/{revId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete214(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/environments/{envId}/revisions/{revId}',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead214(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/environments/{envId}/revisions/{revId}',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set14(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/environments/{envId}/revisions/{revId}',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb15(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/image-name-index',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create215(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/image-name-index',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete215(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/image-name-index',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead215(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/image-name-index',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set15(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/image-name-index',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb16(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/images',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create216(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/images',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete216(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/images',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead216(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/images',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set16(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/images',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb17(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/images-index',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create217(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/images-index',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete217(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/images-index',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead217(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/images-index',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set17(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/images-index',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb18(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/images/{imgid}/url',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create218(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/images/{imgid}/url',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete218(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/images/{imgid}/url',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead218(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/images/{imgid}/url',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set18(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/images/{imgid}/url',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb19(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/os-metadata',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create219(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/os-metadata',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete219(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/os-metadata',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead219(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/os-metadata',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set19(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/os-metadata',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb20(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/patches',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create220(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/patches',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete220(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/patches',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead220(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/patches',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set20(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/patches',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb21(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/patches/{patchId}',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create221(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/patches/{patchId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete221(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/patches/{patchId}',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead221(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/patches/{patchId}',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set21(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/patches/{patchId}',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public migrateDb22(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-repository/templates',
        });
    }

    /**
     * Create a new environment
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public create222(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-repository/templates',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete a specific environment
     * @param envId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public delete222(
        envId: string,
        requestBody?: EnvironmentDeleteRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/environment-repository/templates',
            path: {
                'envId': envId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead222(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/environment-repository/templates',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * Set default environment for a specific operating system ID
     * @param osId
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public set22(
        osId: string,
        envId?: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/environment-repository/templates',
            path: {
                'osId': osId,
            },
            query: {
                'envId': envId,
            },
        });
    }

}
