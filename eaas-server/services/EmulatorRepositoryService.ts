/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EmulatorMetaData } from '../models/EmulatorMetaData.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EmulatorRepositoryService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * List all available emulators
     * @returns EmulatorMetaData
     * @throws ApiError
     */
    public list(): CancelablePromise<Array<EmulatorMetaData>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/emulator-repository/emulators',
        });
    }

    /**
     * Mark emulator version as default
     * @param emuid
     * @returns any Success
     * @throws ApiError
     */
    public makrAsDefault(
        emuid: string,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/emulator-repository/emulators',
            path: {
                'emuid': emuid,
            },
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/emulator-repository/emulators',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * List all available emulators
     * @returns EmulatorMetaData
     * @throws ApiError
     */
    public list1(): CancelablePromise<Array<EmulatorMetaData>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/emulator-repository/emulators/{emuid}/default',
        });
    }

    /**
     * Mark emulator version as default
     * @param emuid
     * @returns any Success
     * @throws ApiError
     */
    public makrAsDefault1(
        emuid: string,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/emulator-repository/emulators/{emuid}/default',
            path: {
                'emuid': emuid,
            },
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead1(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/emulator-repository/emulators/{emuid}/default',
            path: {
                'imgid': imgid,
            },
        });
    }

    /**
     * List all available emulators
     * @returns EmulatorMetaData
     * @throws ApiError
     */
    public list2(): CancelablePromise<Array<EmulatorMetaData>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/emulator-repository/images/{imgid}/url',
        });
    }

    /**
     * Mark emulator version as default
     * @param emuid
     * @returns any Success
     * @throws ApiError
     */
    public makrAsDefault2(
        emuid: string,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/emulator-repository/images/{imgid}/url',
            path: {
                'emuid': emuid,
            },
        });
    }

    /**
     * @param imgid
     * @returns binary
     * @throws ApiError
     */
    public resolveHead2(
        imgid: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/emulator-repository/images/{imgid}/url',
            path: {
                'imgid': imgid,
            },
        });
    }

}
