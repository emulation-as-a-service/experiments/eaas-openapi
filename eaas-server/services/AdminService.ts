/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class AdminService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns binary
     * @throws ApiError
     */
    public getApiKey(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/admin/apikey',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public exportMetadata(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/admin/apikey',
        });
    }

    /**
     * @returns void
     * @throws ApiError
     */
    public resetUsageLog(): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/admin/apikey',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public getApiKey1(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/admin/build-info',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public exportMetadata1(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/admin/build-info',
        });
    }

    /**
     * @returns void
     * @throws ApiError
     */
    public resetUsageLog1(): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/admin/build-info',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public getApiKey2(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/admin/init',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public exportMetadata2(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/admin/init',
        });
    }

    /**
     * @returns void
     * @throws ApiError
     */
    public resetUsageLog2(): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/admin/init',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public getApiKey3(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/admin/metadata-export',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public exportMetadata3(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/admin/metadata-export',
        });
    }

    /**
     * @returns void
     * @throws ApiError
     */
    public resetUsageLog3(): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/admin/metadata-export',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public getApiKey4(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/admin/usage-log',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public exportMetadata4(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/admin/usage-log',
        });
    }

    /**
     * @returns void
     * @throws ApiError
     */
    public resetUsageLog4(): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/admin/usage-log',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public getApiKey5(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/admin/user-info',
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public exportMetadata5(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/admin/user-info',
        });
    }

    /**
     * @returns void
     * @throws ApiError
     */
    public resetUsageLog5(): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/admin/user-info',
        });
    }

}
