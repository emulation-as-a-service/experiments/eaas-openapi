/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ComponentRequest } from '../models/ComponentRequest.ts';
import type { ComponentResponse } from '../models/ComponentResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class ComponentsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent1(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{compid}/{kind}/{resource}/url',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent1(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{compid}/{kind}/{resource}/url',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent1(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{compid}/{kind}/{resource}/url',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead1(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{compid}/{kind}/{resource}/url',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent2(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent2(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent2(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead2(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent3(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/async/checkpoint',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent3(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/async/checkpoint',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent3(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/async/checkpoint',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead3(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/async/checkpoint',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent4(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/async/snapshot',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent4(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/async/snapshot',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent4(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/async/snapshot',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead4(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/async/snapshot',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent5(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/changeMedia',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent5(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/changeMedia',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent5(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/changeMedia',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead5(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/changeMedia',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent6(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/checkpoint',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent6(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/checkpoint',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent6(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/checkpoint',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead6(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/checkpoint',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent7(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/controlurls',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent7(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/controlurls',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent7(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/controlurls',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead7(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/controlurls',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent8(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/downloadPrintJob',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent8(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/downloadPrintJob',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent8(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/downloadPrintJob',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead8(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/downloadPrintJob',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent9(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/events',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent9(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/events',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent9(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/events',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead9(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/events',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent10(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/keepalive',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent10(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/keepalive',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent10(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/keepalive',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead10(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/keepalive',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent11(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/printJobs',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent11(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/printJobs',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent11(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/printJobs',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead11(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/printJobs',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent12(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/result',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent12(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/result',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent12(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/result',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead12(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/result',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent13(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/screenshot',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent13(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/screenshot',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent13(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/screenshot',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead13(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/screenshot',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent14(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/snapshot',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent14(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/snapshot',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent14(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/snapshot',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead14(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/snapshot',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent15(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/state',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent15(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/state',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent15(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/state',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead15(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/state',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

    /**
     * Returns the API representation of the given component.
     * <p>
     * @param componentId The component id to query
     * @returns ComponentResponse The API representation of the component.
     * @throws ApiError
     */
    public getComponent16(
        componentId: string,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/components/{componentId}/stop',
            path: {
                'componentId': componentId,
            },
            errors: {
                500: `on any error, even if the component does not exist`,
            },
        });
    }

    /**
     * Creates and starts a new component (e.g emulator) the EaaS framework.
     * <p>
     * The type of the component is determined by the ComponentRequest's
     * type annotation provided in the POSTed configuration.
     * @param requestBody The configuration for the component to be created.
     * @returns ComponentResponse
     * @throws ApiError
     */
    public createComponent16(
        requestBody?: ComponentRequest,
    ): CancelablePromise<ComponentResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/components/{componentId}/stop',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `if the given component configuration cannot be interpreted`,
                429: `if backend resources are exhausted or quota limits are reached`,
                500: `if the backend was not able to instantiate a new component`,
            },
        });
    }

    /**
     * Release a stopped component session.
     * @param componentId The component's ID to release.
     * @returns void
     * @throws ApiError
     */
    public releaseComponent16(
        componentId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/components/{componentId}/stop',
            path: {
                'componentId': componentId,
            },
        });
    }

    /**
     * @param compid
     * @param kind
     * @param resource
     * @returns binary
     * @throws ApiError
     */
    public resolveResourceHead16(
        compid: string,
        kind: string,
        resource: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'HEAD',
            url: '/components/{componentId}/stop',
            path: {
                'compid': compid,
                'kind': kind,
                'resource': resource,
            },
        });
    }

}
