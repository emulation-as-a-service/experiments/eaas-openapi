/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EmilUserSessionService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param sessionId
     * @returns binary
     * @throws ApiError
     */
    public delete(
        sessionId?: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilUserSession/delete',
            query: {
                'sessionId': sessionId,
            },
        });
    }

    /**
     * @param sessionId
     * @returns binary
     * @throws ApiError
     */
    public delete1(
        sessionId?: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilUserSession/list',
            query: {
                'sessionId': sessionId,
            },
        });
    }

    /**
     * @param sessionId
     * @returns binary
     * @throws ApiError
     */
    public delete2(
        sessionId?: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilUserSession/session',
            query: {
                'sessionId': sessionId,
            },
        });
    }

}
