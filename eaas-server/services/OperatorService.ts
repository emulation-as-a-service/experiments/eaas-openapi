/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class OperatorService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * ### List all configured channels
     * GET http://localhost:8000/operator/api/v1/channels
     * accepts: application/json
     *
     * ### Refresh (reload/download) channels's metadata
     * POST http://localhost:8000/operator/api/v1/channels/example
     * accepts: application/json
     *
     * ### List channels's releases
     * GET http://localhost:8000/operator/api/v1/channels/example/releases
     * accepts: application/json
     *
     * ### Get channels's latest release
     * GET http://localhost:8000/operator/api/v1/channels/example/releases/latest
     * accepts: application/json
     *
     * ### Update to release
     * POST http://localhost:8000/operator/api/v1/channels/example/releases/2.0
     * accepts: application/json
     *
     * ### Get current release
     * GET http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Re-install current release
     * POST http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Get previous release
     * GET http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     *
     * ### Re-install previous release
     * POST http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     * @returns binary
     * @throws ApiError
     */
    public getChannels(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/operator/api/v1/channels',
        });
    }

    /**
     * @param channel
     * @returns binary
     * @throws ApiError
     */
    public reload(
        channel: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/operator/api/v1/channels',
            path: {
                'channel': channel,
            },
        });
    }

    /**
     * ### List all configured channels
     * GET http://localhost:8000/operator/api/v1/channels
     * accepts: application/json
     *
     * ### Refresh (reload/download) channels's metadata
     * POST http://localhost:8000/operator/api/v1/channels/example
     * accepts: application/json
     *
     * ### List channels's releases
     * GET http://localhost:8000/operator/api/v1/channels/example/releases
     * accepts: application/json
     *
     * ### Get channels's latest release
     * GET http://localhost:8000/operator/api/v1/channels/example/releases/latest
     * accepts: application/json
     *
     * ### Update to release
     * POST http://localhost:8000/operator/api/v1/channels/example/releases/2.0
     * accepts: application/json
     *
     * ### Get current release
     * GET http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Re-install current release
     * POST http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Get previous release
     * GET http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     *
     * ### Re-install previous release
     * POST http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     * @returns binary
     * @throws ApiError
     */
    public getChannels1(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/operator/api/v1/channels/{channel}',
        });
    }

    /**
     * @param channel
     * @returns binary
     * @throws ApiError
     */
    public reload1(
        channel: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/operator/api/v1/channels/{channel}',
            path: {
                'channel': channel,
            },
        });
    }

    /**
     * ### List all configured channels
     * GET http://localhost:8000/operator/api/v1/channels
     * accepts: application/json
     *
     * ### Refresh (reload/download) channels's metadata
     * POST http://localhost:8000/operator/api/v1/channels/example
     * accepts: application/json
     *
     * ### List channels's releases
     * GET http://localhost:8000/operator/api/v1/channels/example/releases
     * accepts: application/json
     *
     * ### Get channels's latest release
     * GET http://localhost:8000/operator/api/v1/channels/example/releases/latest
     * accepts: application/json
     *
     * ### Update to release
     * POST http://localhost:8000/operator/api/v1/channels/example/releases/2.0
     * accepts: application/json
     *
     * ### Get current release
     * GET http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Re-install current release
     * POST http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Get previous release
     * GET http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     *
     * ### Re-install previous release
     * POST http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     * @returns binary
     * @throws ApiError
     */
    public getChannels2(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/operator/api/v1/channels/{channel}/releases',
        });
    }

    /**
     * @param channel
     * @returns binary
     * @throws ApiError
     */
    public reload2(
        channel: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/operator/api/v1/channels/{channel}/releases',
            path: {
                'channel': channel,
            },
        });
    }

    /**
     * ### List all configured channels
     * GET http://localhost:8000/operator/api/v1/channels
     * accepts: application/json
     *
     * ### Refresh (reload/download) channels's metadata
     * POST http://localhost:8000/operator/api/v1/channels/example
     * accepts: application/json
     *
     * ### List channels's releases
     * GET http://localhost:8000/operator/api/v1/channels/example/releases
     * accepts: application/json
     *
     * ### Get channels's latest release
     * GET http://localhost:8000/operator/api/v1/channels/example/releases/latest
     * accepts: application/json
     *
     * ### Update to release
     * POST http://localhost:8000/operator/api/v1/channels/example/releases/2.0
     * accepts: application/json
     *
     * ### Get current release
     * GET http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Re-install current release
     * POST http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Get previous release
     * GET http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     *
     * ### Re-install previous release
     * POST http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     * @returns binary
     * @throws ApiError
     */
    public getChannels3(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/operator/api/v1/channels/{channel}/releases/latest',
        });
    }

    /**
     * @param channel
     * @returns binary
     * @throws ApiError
     */
    public reload3(
        channel: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/operator/api/v1/channels/{channel}/releases/latest',
            path: {
                'channel': channel,
            },
        });
    }

    /**
     * ### List all configured channels
     * GET http://localhost:8000/operator/api/v1/channels
     * accepts: application/json
     *
     * ### Refresh (reload/download) channels's metadata
     * POST http://localhost:8000/operator/api/v1/channels/example
     * accepts: application/json
     *
     * ### List channels's releases
     * GET http://localhost:8000/operator/api/v1/channels/example/releases
     * accepts: application/json
     *
     * ### Get channels's latest release
     * GET http://localhost:8000/operator/api/v1/channels/example/releases/latest
     * accepts: application/json
     *
     * ### Update to release
     * POST http://localhost:8000/operator/api/v1/channels/example/releases/2.0
     * accepts: application/json
     *
     * ### Get current release
     * GET http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Re-install current release
     * POST http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Get previous release
     * GET http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     *
     * ### Re-install previous release
     * POST http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     * @returns binary
     * @throws ApiError
     */
    public getChannels4(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/operator/api/v1/channels/{channel}/releases/{version}',
        });
    }

    /**
     * @param channel
     * @returns binary
     * @throws ApiError
     */
    public reload4(
        channel: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/operator/api/v1/channels/{channel}/releases/{version}',
            path: {
                'channel': channel,
            },
        });
    }

    /**
     * ### List all configured channels
     * GET http://localhost:8000/operator/api/v1/channels
     * accepts: application/json
     *
     * ### Refresh (reload/download) channels's metadata
     * POST http://localhost:8000/operator/api/v1/channels/example
     * accepts: application/json
     *
     * ### List channels's releases
     * GET http://localhost:8000/operator/api/v1/channels/example/releases
     * accepts: application/json
     *
     * ### Get channels's latest release
     * GET http://localhost:8000/operator/api/v1/channels/example/releases/latest
     * accepts: application/json
     *
     * ### Update to release
     * POST http://localhost:8000/operator/api/v1/channels/example/releases/2.0
     * accepts: application/json
     *
     * ### Get current release
     * GET http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Re-install current release
     * POST http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Get previous release
     * GET http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     *
     * ### Re-install previous release
     * POST http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     * @returns binary
     * @throws ApiError
     */
    public getChannels5(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/operator/api/v1/deployment/current',
        });
    }

    /**
     * @param channel
     * @returns binary
     * @throws ApiError
     */
    public reload5(
        channel: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/operator/api/v1/deployment/current',
            path: {
                'channel': channel,
            },
        });
    }

    /**
     * ### List all configured channels
     * GET http://localhost:8000/operator/api/v1/channels
     * accepts: application/json
     *
     * ### Refresh (reload/download) channels's metadata
     * POST http://localhost:8000/operator/api/v1/channels/example
     * accepts: application/json
     *
     * ### List channels's releases
     * GET http://localhost:8000/operator/api/v1/channels/example/releases
     * accepts: application/json
     *
     * ### Get channels's latest release
     * GET http://localhost:8000/operator/api/v1/channels/example/releases/latest
     * accepts: application/json
     *
     * ### Update to release
     * POST http://localhost:8000/operator/api/v1/channels/example/releases/2.0
     * accepts: application/json
     *
     * ### Get current release
     * GET http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Re-install current release
     * POST http://localhost:8000/operator/api/v1/deployment/current
     * accepts: application/json
     *
     * ### Get previous release
     * GET http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     *
     * ### Re-install previous release
     * POST http://localhost:8000/operator/api/v1/deployment/previous
     * accepts: application/json
     * @returns binary
     * @throws ApiError
     */
    public getChannels6(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/operator/api/v1/deployment/previous',
        });
    }

    /**
     * @param channel
     * @returns binary
     * @throws ApiError
     */
    public reload6(
        channel: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/operator/api/v1/deployment/previous',
            path: {
                'channel': channel,
            },
        });
    }

}
