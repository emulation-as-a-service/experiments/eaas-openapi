/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class ErrorReportService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns binary
     * @throws ApiError
     */
    public getErrorReport(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/error-report',
        });
    }

}
