/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Proposal } from '../models/Proposal.ts';
import type { ProposalRequest } from '../models/ProposalRequest.ts';
import type { ProposalResponse } from '../models/ProposalResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EnvironmentProposerService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Get and remove a proposal resource
     * @param id
     * @returns Proposal
     * @throws ApiError
     */
    public getProposal2(
        id: string,
    ): CancelablePromise<Proposal> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-proposer/api/v2/proposals',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Submit a new proposal task
     * @param requestBody
     * @returns ProposalResponse If task was submitted successfully.
     * @throws ApiError
     */
    public postProposal2(
        requestBody?: ProposalRequest,
    ): CancelablePromise<ProposalResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-proposer/api/v2/proposals',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                500: `If other internal errors occure.`,
                503: `If server is out of resources. Request should be retried later.`,
            },
        });
    }

    /**
     * Get and remove a proposal resource
     * @param id
     * @returns Proposal
     * @throws ApiError
     */
    public getProposal21(
        id: string,
    ): CancelablePromise<Proposal> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-proposer/api/v2/proposals/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Submit a new proposal task
     * @param requestBody
     * @returns ProposalResponse If task was submitted successfully.
     * @throws ApiError
     */
    public postProposal21(
        requestBody?: ProposalRequest,
    ): CancelablePromise<ProposalResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-proposer/api/v2/proposals/{id}',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                500: `If other internal errors occure.`,
                503: `If server is out of resources. Request should be retried later.`,
            },
        });
    }

    /**
     * Get and remove a proposal resource
     * @param id
     * @returns Proposal
     * @throws ApiError
     */
    public getProposal22(
        id: string,
    ): CancelablePromise<Proposal> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/environment-proposer/api/v2/waitqueue/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Submit a new proposal task
     * @param requestBody
     * @returns ProposalResponse If task was submitted successfully.
     * @throws ApiError
     */
    public postProposal22(
        requestBody?: ProposalRequest,
    ): CancelablePromise<ProposalResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/environment-proposer/api/v2/waitqueue/{id}',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                500: `If other internal errors occure.`,
                503: `If server is out of resources. Request should be retried later.`,
            },
        });
    }

}
