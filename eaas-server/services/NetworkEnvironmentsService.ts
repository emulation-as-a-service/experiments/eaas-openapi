/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { NetworkEnvironment } from '../models/NetworkEnvironment.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class NetworkEnvironmentsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns binary
     * @throws ApiError
     */
    public getNetworkEnvironments(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/network-environments',
        });
    }

    /**
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public createNetworkEnvironment(
        requestBody?: NetworkEnvironment,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/network-environments',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public updateNetworkEnvironment(
        requestBody?: NetworkEnvironment,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/network-environments',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public deleteNetworkEnvironment(
        envId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/network-environments',
            path: {
                'envId': envId,
            },
        });
    }

    /**
     * @returns binary
     * @throws ApiError
     */
    public getNetworkEnvironments1(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/network-environments/{envId}',
        });
    }

    /**
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public createNetworkEnvironment1(
        requestBody?: NetworkEnvironment,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/network-environments/{envId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public updateNetworkEnvironment1(
        requestBody?: NetworkEnvironment,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/network-environments/{envId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param envId
     * @returns void
     * @throws ApiError
     */
    public deleteNetworkEnvironment1(
        envId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/network-environments/{envId}',
            path: {
                'envId': envId,
            },
        });
    }

}
