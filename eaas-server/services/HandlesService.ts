/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { HandleListResponse } from '../models/HandleListResponse.ts';
import type { HandleRequest } from '../models/HandleRequest.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class HandlesService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns HandleListResponse
     * @throws ApiError
     */
    public getHandleList(): CancelablePromise<HandleListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/handles',
        });
    }

    /**
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createHandle(
        requestBody?: HandleRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/handles',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param handle
     * @returns void
     * @throws ApiError
     */
    public deleteHandle(
        handle: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/handles',
            path: {
                'handle': handle,
            },
        });
    }

    /**
     * @returns HandleListResponse
     * @throws ApiError
     */
    public getHandleList1(): CancelablePromise<HandleListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/handles/{handle}',
        });
    }

    /**
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createHandle1(
        requestBody?: HandleRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/handles/{handle}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param handle
     * @returns void
     * @throws ApiError
     */
    public deleteHandle1(
        handle: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/handles/{handle}',
            path: {
                'handle': handle,
            },
        });
    }

    /**
     * @returns HandleListResponse
     * @throws ApiError
     */
    public getHandleList2(): CancelablePromise<HandleListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/handles/{prefix}/{handle}',
        });
    }

    /**
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createHandle2(
        requestBody?: HandleRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/handles/{prefix}/{handle}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param handle
     * @returns void
     * @throws ApiError
     */
    public deleteHandle2(
        handle: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/handles/{prefix}/{handle}',
            path: {
                'handle': handle,
            },
        });
    }

}
