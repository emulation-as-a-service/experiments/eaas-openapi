/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateContainerImageRequest } from '../models/CreateContainerImageRequest.ts';
import type { RuntimeListResponse } from '../models/RuntimeListResponse.ts';
import type { TaskStateResponse } from '../models/TaskStateResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EmilContainerDataService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Get List of available Runtimes
     * @returns RuntimeListResponse List of Container Runtimes
     * @throws ApiError
     */
    public getOriginRuntimeList(): CancelablePromise<RuntimeListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilContainerData/buildContainerImage',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public saveContainerImage(
        requestBody?: CreateContainerImageRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilContainerData/buildContainerImage',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get List of available Runtimes
     * @returns RuntimeListResponse List of Container Runtimes
     * @throws ApiError
     */
    public getOriginRuntimeList1(): CancelablePromise<RuntimeListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilContainerData/delete',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public saveContainerImage1(
        requestBody?: CreateContainerImageRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilContainerData/delete',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get List of available Runtimes
     * @returns RuntimeListResponse List of Container Runtimes
     * @throws ApiError
     */
    public getOriginRuntimeList2(): CancelablePromise<RuntimeListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilContainerData/getOriginRuntimeList',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public saveContainerImage2(
        requestBody?: CreateContainerImageRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilContainerData/getOriginRuntimeList',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get List of available Runtimes
     * @returns RuntimeListResponse List of Container Runtimes
     * @throws ApiError
     */
    public getOriginRuntimeList3(): CancelablePromise<RuntimeListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilContainerData/importContainer',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public saveContainerImage3(
        requestBody?: CreateContainerImageRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilContainerData/importContainer',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get List of available Runtimes
     * @returns RuntimeListResponse List of Container Runtimes
     * @throws ApiError
     */
    public getOriginRuntimeList4(): CancelablePromise<RuntimeListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilContainerData/importEmulator',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public saveContainerImage4(
        requestBody?: CreateContainerImageRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilContainerData/importEmulator',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get List of available Runtimes
     * @returns RuntimeListResponse List of Container Runtimes
     * @throws ApiError
     */
    public getOriginRuntimeList5(): CancelablePromise<RuntimeListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilContainerData/updateContainer',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public saveContainerImage5(
        requestBody?: CreateContainerImageRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilContainerData/updateContainer',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get List of available Runtimes
     * @returns RuntimeListResponse List of Container Runtimes
     * @throws ApiError
     */
    public getOriginRuntimeList6(): CancelablePromise<RuntimeListResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilContainerData/updateLatestEmulator',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public saveContainerImage6(
        requestBody?: CreateContainerImageRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilContainerData/updateLatestEmulator',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
