/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EmilService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public buildInfo(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/Emil/buildInfo',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public buildInfo1(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/Emil/exportMetadata',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public buildInfo2(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/Emil/resetUsageLog',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public buildInfo3(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/Emil/usageLog',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public buildInfo4(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/Emil/userInfo',
        });
    }

}
