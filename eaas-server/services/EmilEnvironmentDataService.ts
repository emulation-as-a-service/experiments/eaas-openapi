/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EnvironmentCreateRequest } from '../models/EnvironmentCreateRequest.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EmilEnvironmentDataService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments1(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/createEnvironment',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment1(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/createEnvironment',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments2(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/defaultEnvironment',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment2(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/defaultEnvironment',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments3(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/defaultEnvironments',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment3(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/defaultEnvironments',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments4(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/delete',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment4(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/delete',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments5(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/export',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment5(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/export',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments6(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/forkRevision',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment6(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/forkRevision',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments7(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/getEnvironmentTemplates',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment7(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/getEnvironmentTemplates',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments8(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/getNameIndexes',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment8(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/getNameIndexes',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments9(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/getPatches',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment9(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/getPatches',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments10(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/importImage',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment10(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/importImage',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments11(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/init',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment11(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/init',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments12(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/objectDependencies',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment12(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/objectDependencies',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments13(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/operatingSystemMetadata',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment13(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/operatingSystemMetadata',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments14(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/replicateImage',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment14(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/replicateImage',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments15(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/revertRevision',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment15(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/revertRevision',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments16(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/setDefaultEnvironment',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment16(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/setDefaultEnvironment',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments17(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/sync',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment17(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/sync',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments18(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/updateDescription',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment18(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/updateDescription',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @returns binary
     * @throws ApiError
     */
    public getEnvironments19(): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilEnvironmentData/{envId}',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns binary
     * @throws ApiError
     */
    public createEnvironment19(
        requestBody?: EnvironmentCreateRequest,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilEnvironmentData/{envId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
