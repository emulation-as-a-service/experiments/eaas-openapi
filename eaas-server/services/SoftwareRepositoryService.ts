/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EmilSoftwareObject } from '../models/EmilSoftwareObject.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class SoftwareRepositoryService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Looks up and returns the descriptions for all software packages.
     * A JSON response will be returned, containing:
     * <pre>
     * {
         * "status": "0",
         * "descriptions": [
             * { "id": &ltSoftwarePackage's ID&gt, "label": "Short description" },
             * ...
             * ]
             * }
             * </pre>
             *
             * When an internal error occurs, a JSON response containing
             * the corresponding message will be returned:
             * <pre>
             * {
                 * "status": "2",
                 * "message": "Error message."
                 * }
                 * </pre>
                 * @returns binary A JSON response containing a list of descriptions
                 * for all software packages or an error message.
                 * @throws ApiError
                 */
                public list5(): CancelablePromise<Blob> {
                    return this.httpRequest.request({
                        method: 'GET',
                        url: '/software-repository/descriptions',
                    });
                }

                /**
                 * Save or update software object meta data.
                 * expects a JSON object:
                 * <pre>
                 * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
                 * </pre>
                 * @param requestBody EmilSoftwareObject as JSON string
                 * @returns binary JSON response (error) message
                 * @throws ApiError
                 */
                public create3(
                    requestBody?: EmilSoftwareObject,
                ): CancelablePromise<Blob> {
                    return this.httpRequest.request({
                        method: 'POST',
                        url: '/software-repository/descriptions',
                        body: requestBody,
                        mediaType: 'application/json',
                    });
                }

                /**
                 * @param softwareId
                 * @returns void
                 * @throws ApiError
                 */
                public deleteSoftware(
                    softwareId: string,
                ): CancelablePromise<void> {
                    return this.httpRequest.request({
                        method: 'DELETE',
                        url: '/software-repository/descriptions',
                        path: {
                            'softwareId': softwareId,
                        },
                    });
                }

                /**
                 * Looks up and returns the descriptions for all software packages.
                 * A JSON response will be returned, containing:
                 * <pre>
                 * {
                     * "status": "0",
                     * "descriptions": [
                         * { "id": &ltSoftwarePackage's ID&gt, "label": "Short description" },
                         * ...
                         * ]
                         * }
                         * </pre>
                         *
                         * When an internal error occurs, a JSON response containing
                         * the corresponding message will be returned:
                         * <pre>
                         * {
                             * "status": "2",
                             * "message": "Error message."
                             * }
                             * </pre>
                             * @returns binary A JSON response containing a list of descriptions
                             * for all software packages or an error message.
                             * @throws ApiError
                             */
                            public list51(): CancelablePromise<Blob> {
                                return this.httpRequest.request({
                                    method: 'GET',
                                    url: '/software-repository/descriptions/{softwareId}',
                                });
                            }

                            /**
                             * Save or update software object meta data.
                             * expects a JSON object:
                             * <pre>
                             * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
                             * </pre>
                             * @param requestBody EmilSoftwareObject as JSON string
                             * @returns binary JSON response (error) message
                             * @throws ApiError
                             */
                            public create31(
                                requestBody?: EmilSoftwareObject,
                            ): CancelablePromise<Blob> {
                                return this.httpRequest.request({
                                    method: 'POST',
                                    url: '/software-repository/descriptions/{softwareId}',
                                    body: requestBody,
                                    mediaType: 'application/json',
                                });
                            }

                            /**
                             * @param softwareId
                             * @returns void
                             * @throws ApiError
                             */
                            public deleteSoftware1(
                                softwareId: string,
                            ): CancelablePromise<void> {
                                return this.httpRequest.request({
                                    method: 'DELETE',
                                    url: '/software-repository/descriptions/{softwareId}',
                                    path: {
                                        'softwareId': softwareId,
                                    },
                                });
                            }

                            /**
                             * Looks up and returns the descriptions for all software packages.
                             * A JSON response will be returned, containing:
                             * <pre>
                             * {
                                 * "status": "0",
                                 * "descriptions": [
                                     * { "id": &ltSoftwarePackage's ID&gt, "label": "Short description" },
                                     * ...
                                     * ]
                                     * }
                                     * </pre>
                                     *
                                     * When an internal error occurs, a JSON response containing
                                     * the corresponding message will be returned:
                                     * <pre>
                                     * {
                                         * "status": "2",
                                         * "message": "Error message."
                                         * }
                                         * </pre>
                                         * @returns binary A JSON response containing a list of descriptions
                                         * for all software packages or an error message.
                                         * @throws ApiError
                                         */
                                        public list52(): CancelablePromise<Blob> {
                                            return this.httpRequest.request({
                                                method: 'GET',
                                                url: '/software-repository/packages',
                                            });
                                        }

                                        /**
                                         * Save or update software object meta data.
                                         * expects a JSON object:
                                         * <pre>
                                         * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
                                         * </pre>
                                         * @param requestBody EmilSoftwareObject as JSON string
                                         * @returns binary JSON response (error) message
                                         * @throws ApiError
                                         */
                                        public create32(
                                            requestBody?: EmilSoftwareObject,
                                        ): CancelablePromise<Blob> {
                                            return this.httpRequest.request({
                                                method: 'POST',
                                                url: '/software-repository/packages',
                                                body: requestBody,
                                                mediaType: 'application/json',
                                            });
                                        }

                                        /**
                                         * @param softwareId
                                         * @returns void
                                         * @throws ApiError
                                         */
                                        public deleteSoftware2(
                                            softwareId: string,
                                        ): CancelablePromise<void> {
                                            return this.httpRequest.request({
                                                method: 'DELETE',
                                                url: '/software-repository/packages',
                                                path: {
                                                    'softwareId': softwareId,
                                                },
                                            });
                                        }

                                        /**
                                         * Looks up and returns the descriptions for all software packages.
                                         * A JSON response will be returned, containing:
                                         * <pre>
                                         * {
                                             * "status": "0",
                                             * "descriptions": [
                                                 * { "id": &ltSoftwarePackage's ID&gt, "label": "Short description" },
                                                 * ...
                                                 * ]
                                                 * }
                                                 * </pre>
                                                 *
                                                 * When an internal error occurs, a JSON response containing
                                                 * the corresponding message will be returned:
                                                 * <pre>
                                                 * {
                                                     * "status": "2",
                                                     * "message": "Error message."
                                                     * }
                                                     * </pre>
                                                     * @returns binary A JSON response containing a list of descriptions
                                                     * for all software packages or an error message.
                                                     * @throws ApiError
                                                     */
                                                    public list53(): CancelablePromise<Blob> {
                                                        return this.httpRequest.request({
                                                            method: 'GET',
                                                            url: '/software-repository/packages/{softwareId}',
                                                        });
                                                    }

                                                    /**
                                                     * Save or update software object meta data.
                                                     * expects a JSON object:
                                                     * <pre>
                                                     * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
                                                     * </pre>
                                                     * @param requestBody EmilSoftwareObject as JSON string
                                                     * @returns binary JSON response (error) message
                                                     * @throws ApiError
                                                     */
                                                    public create33(
                                                        requestBody?: EmilSoftwareObject,
                                                    ): CancelablePromise<Blob> {
                                                        return this.httpRequest.request({
                                                            method: 'POST',
                                                            url: '/software-repository/packages/{softwareId}',
                                                            body: requestBody,
                                                            mediaType: 'application/json',
                                                        });
                                                    }

                                                    /**
                                                     * @param softwareId
                                                     * @returns void
                                                     * @throws ApiError
                                                     */
                                                    public deleteSoftware3(
                                                        softwareId: string,
                                                    ): CancelablePromise<void> {
                                                        return this.httpRequest.request({
                                                            method: 'DELETE',
                                                            url: '/software-repository/packages/{softwareId}',
                                                            path: {
                                                                'softwareId': softwareId,
                                                            },
                                                        });
                                                    }

                                                }
