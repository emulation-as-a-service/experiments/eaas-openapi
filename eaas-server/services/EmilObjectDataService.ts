/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ChangeObjectLabelRequest } from '../models/ChangeObjectLabelRequest.ts';
import type { ImportObjectRequest } from '../models/ImportObjectRequest.ts';
import type { ObjectArchivesResponse } from '../models/ObjectArchivesResponse.ts';
import type { TaskStateResponse } from '../models/TaskStateResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EmilObjectDataService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/archives',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/archives',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject2(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/archives',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete4(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/archives',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives1(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/import',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription1(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/import',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject21(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/import',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete41(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/import',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives2(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/objectImportTaskState',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription2(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/objectImportTaskState',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject22(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/objectImportTaskState',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete42(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/objectImportTaskState',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives3(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/sync',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription3(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/sync',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject23(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/sync',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete43(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/sync',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives4(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/syncObjects',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription4(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/syncObjects',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject24(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/syncObjects',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete44(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/syncObjects',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives5(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/{objectArchive}',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription5(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/{objectArchive}',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject25(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/{objectArchive}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete45(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/{objectArchive}',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives6(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/{objectArchive}/{objectId}',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription6(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/{objectArchive}/{objectId}',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject26(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/{objectArchive}/{objectId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete46(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/{objectArchive}/{objectId}',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

    /**
     * @deprecated
     * @returns ObjectArchivesResponse
     * @throws ApiError
     */
    public getArchives7(): CancelablePromise<ObjectArchivesResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/objects/{objectArchive}/{objectId}/label',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public mediaDescription7(
        objectArchive: string,
        objectId: string,
        requestBody?: ChangeObjectLabelRequest,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/objects/{objectArchive}/{objectId}/label',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public importObject27(
        requestBody?: ImportObjectRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/objects/{objectArchive}/{objectId}/label',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param objectArchive
     * @param objectId
     * @returns void
     * @throws ApiError
     */
    public delete47(
        objectArchive: string,
        objectId: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/objects/{objectArchive}/{objectId}/label',
            path: {
                'objectArchive': objectArchive,
                'objectId': objectId,
            },
        });
    }

}
