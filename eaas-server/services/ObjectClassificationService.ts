/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ClientClassificationRequest } from '../models/ClientClassificationRequest.ts';
import type { TaskStateResponse } from '../models/TaskStateResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class ObjectClassificationService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public classify(
        requestBody?: ClientClassificationRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/classification',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public classify1(
        requestBody?: ClientClassificationRequest,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/classification/overrideObjectCharacterization',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
