/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { NetworkRequest } from '../models/NetworkRequest.ts';
import type { NetworkResponse } from '../models/NetworkResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class NetworksService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param id
     * @returns binary
     * @throws ApiError
     */
    public wsConnection(
        id: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/networks',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns NetworkResponse
     * @throws ApiError
     */
    public createNetwork(
        requestBody?: NetworkRequest,
    ): CancelablePromise<NetworkResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/networks',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param componentId
     * @param id
     * @returns void
     * @throws ApiError
     */
    public removeComponent(
        componentId: string,
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/networks',
            path: {
                'componentId': componentId,
                'id': id,
            },
        });
    }

    /**
     * @param id
     * @returns binary
     * @throws ApiError
     */
    public wsConnection1(
        id: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/networks/{id}/addComponentToSwitch',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns NetworkResponse
     * @throws ApiError
     */
    public createNetwork1(
        requestBody?: NetworkRequest,
    ): CancelablePromise<NetworkResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/networks/{id}/addComponentToSwitch',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param componentId
     * @param id
     * @returns void
     * @throws ApiError
     */
    public removeComponent1(
        componentId: string,
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/networks/{id}/addComponentToSwitch',
            path: {
                'componentId': componentId,
                'id': id,
            },
        });
    }

    /**
     * @param id
     * @returns binary
     * @throws ApiError
     */
    public wsConnection2(
        id: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/networks/{id}/components',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns NetworkResponse
     * @throws ApiError
     */
    public createNetwork2(
        requestBody?: NetworkRequest,
    ): CancelablePromise<NetworkResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/networks/{id}/components',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param componentId
     * @param id
     * @returns void
     * @throws ApiError
     */
    public removeComponent2(
        componentId: string,
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/networks/{id}/components',
            path: {
                'componentId': componentId,
                'id': id,
            },
        });
    }

    /**
     * @param id
     * @returns binary
     * @throws ApiError
     */
    public wsConnection3(
        id: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/networks/{id}/components/{componentId}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns NetworkResponse
     * @throws ApiError
     */
    public createNetwork3(
        requestBody?: NetworkRequest,
    ): CancelablePromise<NetworkResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/networks/{id}/components/{componentId}',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param componentId
     * @param id
     * @returns void
     * @throws ApiError
     */
    public removeComponent3(
        componentId: string,
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/networks/{id}/components/{componentId}',
            path: {
                'componentId': componentId,
                'id': id,
            },
        });
    }

    /**
     * @param id
     * @returns binary
     * @throws ApiError
     */
    public wsConnection4(
        id: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/networks/{id}/components/{componentId}/disconnect',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns NetworkResponse
     * @throws ApiError
     */
    public createNetwork4(
        requestBody?: NetworkRequest,
    ): CancelablePromise<NetworkResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/networks/{id}/components/{componentId}/disconnect',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param componentId
     * @param id
     * @returns void
     * @throws ApiError
     */
    public removeComponent4(
        componentId: string,
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/networks/{id}/components/{componentId}/disconnect',
            path: {
                'componentId': componentId,
                'id': id,
            },
        });
    }

    /**
     * @param id
     * @returns binary
     * @throws ApiError
     */
    public wsConnection5(
        id: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/networks/{id}/wsConnection',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns NetworkResponse
     * @throws ApiError
     */
    public createNetwork5(
        requestBody?: NetworkRequest,
    ): CancelablePromise<NetworkResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/networks/{id}/wsConnection',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param componentId
     * @param id
     * @returns void
     * @throws ApiError
     */
    public removeComponent5(
        componentId: string,
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/networks/{id}/wsConnection',
            path: {
                'componentId': componentId,
                'id': id,
            },
        });
    }

}
