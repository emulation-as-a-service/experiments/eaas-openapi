/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { DetachRequest } from '../models/DetachRequest.ts';
import type { Session } from '../models/Session.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class SessionsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns Session
     * @throws ApiError
     */
    public list4(): CancelablePromise<Array<Session>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/sessions',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any Success
     * @throws ApiError
     */
    public setLifetime(
        id: string,
        requestBody?: DetachRequest,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/sessions',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    public delete5(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/sessions',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @returns Session
     * @throws ApiError
     */
    public list41(): CancelablePromise<Array<Session>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/sessions/network-environments',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any Success
     * @throws ApiError
     */
    public setLifetime1(
        id: string,
        requestBody?: DetachRequest,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/sessions/network-environments',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    public delete51(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/sessions/network-environments',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @returns Session
     * @throws ApiError
     */
    public list42(): CancelablePromise<Array<Session>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/sessions/{id}',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any Success
     * @throws ApiError
     */
    public setLifetime2(
        id: string,
        requestBody?: DetachRequest,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/sessions/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    public delete52(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/sessions/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @returns Session
     * @throws ApiError
     */
    public list43(): CancelablePromise<Array<Session>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/sessions/{id}/detach',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any Success
     * @throws ApiError
     */
    public setLifetime3(
        id: string,
        requestBody?: DetachRequest,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/sessions/{id}/detach',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    public delete53(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/sessions/{id}/detach',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @returns Session
     * @throws ApiError
     */
    public list44(): CancelablePromise<Array<Session>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/sessions/{id}/keepalive',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any Success
     * @throws ApiError
     */
    public setLifetime4(
        id: string,
        requestBody?: DetachRequest,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/sessions/{id}/keepalive',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    public delete54(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/sessions/{id}/keepalive',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @returns Session
     * @throws ApiError
     */
    public list45(): CancelablePromise<Array<Session>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/sessions/{id}/resources',
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any Success
     * @throws ApiError
     */
    public setLifetime5(
        id: string,
        requestBody?: DetachRequest,
    ): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/sessions/{id}/resources',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    public delete55(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/sessions/{id}/resources',
            path: {
                'id': id,
            },
        });
    }

}
