/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { TaskStateResponse } from '../models/TaskStateResponse.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class TaskManagerService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param id
     * @param cleanup
     * @returns TaskStateResponse
     * @throws ApiError
     */
    public lookup(
        id: string,
        cleanup: boolean = true,
    ): CancelablePromise<TaskStateResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/tasks/{id}',
            path: {
                'id': id,
            },
            query: {
                'cleanup': cleanup,
            },
        });
    }

    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    public remove(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/tasks/{id}',
            path: {
                'id': id,
            },
        });
    }

}
