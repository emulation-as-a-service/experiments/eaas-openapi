/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { EmilSoftwareObject } from '../models/EmilSoftwareObject.ts';

import type { CancelablePromise } from '../core/CancelablePromise.ts';
import type { BaseHttpRequest } from '../core/BaseHttpRequest.ts';

export class EmilSoftwareDataService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @deprecated
     * @param softwareId
     * @returns binary
     * @throws ApiError
     */
    public getSoftwareObject(
        softwareId?: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilSoftwareData/getSoftwareObject',
            query: {
                'softwareId': softwareId,
            },
        });
    }

    /**
     * @deprecated
     * saves or updates software object meta data.
     * expects a JSON object:
     * <pre>
     * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
     * </pre>
     * @param requestBody EmilSoftwareObject as JSON string
     * @returns binary JSON response (error) message
     * @throws ApiError
     */
    public saveSoftwareObject(
        requestBody?: EmilSoftwareObject,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilSoftwareData/getSoftwareObject',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param softwareId
     * @returns binary
     * @throws ApiError
     */
    public getSoftwareObject1(
        softwareId?: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilSoftwareData/getSoftwarePackageDescription',
            query: {
                'softwareId': softwareId,
            },
        });
    }

    /**
     * @deprecated
     * saves or updates software object meta data.
     * expects a JSON object:
     * <pre>
     * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
     * </pre>
     * @param requestBody EmilSoftwareObject as JSON string
     * @returns binary JSON response (error) message
     * @throws ApiError
     */
    public saveSoftwareObject1(
        requestBody?: EmilSoftwareObject,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilSoftwareData/getSoftwarePackageDescription',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param softwareId
     * @returns binary
     * @throws ApiError
     */
    public getSoftwareObject2(
        softwareId?: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilSoftwareData/getSoftwarePackageDescriptions',
            query: {
                'softwareId': softwareId,
            },
        });
    }

    /**
     * @deprecated
     * saves or updates software object meta data.
     * expects a JSON object:
     * <pre>
     * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
     * </pre>
     * @param requestBody EmilSoftwareObject as JSON string
     * @returns binary JSON response (error) message
     * @throws ApiError
     */
    public saveSoftwareObject2(
        requestBody?: EmilSoftwareObject,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilSoftwareData/getSoftwarePackageDescriptions',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @deprecated
     * @param softwareId
     * @returns binary
     * @throws ApiError
     */
    public getSoftwareObject3(
        softwareId?: string,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/EmilSoftwareData/saveSoftwareObject',
            query: {
                'softwareId': softwareId,
            },
        });
    }

    /**
     * @deprecated
     * saves or updates software object meta data.
     * expects a JSON object:
     * <pre>
     * {"objectId":"id","licenseInformation":"","allowedInstances":1,"nativeFMTs":[],"importFMTs":[],"exportFMTs":[]}
     * </pre>
     * @param requestBody EmilSoftwareObject as JSON string
     * @returns binary JSON response (error) message
     * @throws ApiError
     */
    public saveSoftwareObject3(
        requestBody?: EmilSoftwareObject,
    ): CancelablePromise<Blob> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/EmilSoftwareData/saveSoftwareObject',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
