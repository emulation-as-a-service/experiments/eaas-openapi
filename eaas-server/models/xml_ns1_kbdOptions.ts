/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns1_kbdOptions = {
    required?: boolean;
    clientKbdLayout?: string;
    clientKbdModel?: string;
    emulatorKbdLayout?: string;
    emulatorKbdModel?: string;
};

