/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type InputOptions = {
    clientKbdLayout?: string;
    clientKbdModel?: string;
    emulatorKbdLayout?: string;
    emulatorKbdModel?: string;
    required: boolean;
};

