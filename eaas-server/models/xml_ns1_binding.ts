/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_anonymous_AccessType } from './xml_ns0_anonymous_AccessType.ts';
import type { xml_ns0_anonymous_TransportType } from './xml_ns0_anonymous_TransportType.ts';
import type { xml_ns1_abstractDataResource } from './xml_ns1_abstractDataResource.ts';

export type xml_ns1_binding = (xml_ns1_abstractDataResource & {
    url?: string;
    transport?: xml_ns0_anonymous_TransportType;
    access?: xml_ns0_anonymous_AccessType;
    localAlias?: string;
    filesize?: number;
    imageType?: string;
    username?: string;
    password?: string;
});

