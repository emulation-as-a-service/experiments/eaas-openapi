/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_URL } from './xml_ns0_URL.ts';

export type xml_ns0_uploadedItem = {
    filename?: string;
    url?: xml_ns0_URL;
};

