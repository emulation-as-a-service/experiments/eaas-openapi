/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Aliases } from './Aliases.ts';
import type { Entries } from './Entries.ts';

export type ImageNameIndex = {
    entries?: Entries;
    aliases?: Aliases;
};

