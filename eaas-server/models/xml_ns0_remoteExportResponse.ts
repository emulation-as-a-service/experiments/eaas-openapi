/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_taskStateResponse } from './xml_ns0_taskStateResponse.ts';

export type xml_ns0_remoteExportResponse = xml_ns0_taskStateResponse;

