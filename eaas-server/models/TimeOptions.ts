/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TimeOptions = {
    offset?: string;
    epoch?: string;
};

