/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SmLink } from './SmLink.ts';
import type { SmLinkGrp } from './SmLinkGrp.ts';

export type StructLinkType = {
    smLink?: Array<SmLink>;
    smLinkGrp?: Array<SmLinkGrp>;
    ID?: string;
};

