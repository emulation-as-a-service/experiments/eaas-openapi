/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FContent } from './FContent.ts';
import type { FLocat } from './FLocat.ts';
import type { Stream } from './Stream.ts';
import type { TransformFile } from './TransformFile.ts';

export type FileType = {
    FLocat?: Array<FLocat>;
    FContent?: FContent;
    stream?: Array<Stream>;
    transformFile?: Array<TransformFile>;
    file?: Array<FileType>;
    ID?: string;
    END?: string;
    SEQ?: number;
    USE?: string;
    SIZE?: number;
    ADMID?: Array<any>;
    BEGIN?: string;
    DMDID?: Array<any>;
    GROUPID?: string;
    CHECKSUMTYPE?: string;
    CREATED?: number;
    OWNERID?: string;
    MIMETYPE?: string;
    BETYPE?: string;
    CHECKSUM?: string;
};

