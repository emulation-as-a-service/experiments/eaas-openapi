/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_process } from './xml_ns0_process.ts';
import type { xml_ns1_containerConfiguration } from './xml_ns1_containerConfiguration.ts';

export type xml_ns1_ociContainerConfiguration = (xml_ns1_containerConfiguration & {
    process?: xml_ns0_process;
    rootfs?: string;
    isGui?: boolean;
    customSubdir?: string;
});

