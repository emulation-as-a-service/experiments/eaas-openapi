/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentConfiguration } from './ComponentConfiguration.ts';

export type NetworkSwitchConfiguration = ComponentConfiguration;

