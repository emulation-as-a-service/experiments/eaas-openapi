/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_areaType } from './xml_ns3_areaType.ts';
import type { xml_ns3_parType } from './xml_ns3_parType.ts';

export type xml_ns3_seqType = {
    ID?: string;
    area?: xml_ns3_areaType;
    par?: xml_ns3_parType;
};

