/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum DataType {
    ZIP = 'zip',
    TAR = 'tar',
    BAGIT_ZIP = 'bagit+zip',
    BAGIT_TAR = 'bagit+tar',
}
