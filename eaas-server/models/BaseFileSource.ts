/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Action } from './Action.ts';
import type { ArchiveFormat } from './ArchiveFormat.ts';
import type { JaxbType } from './JaxbType.ts';

export type BaseFileSource = (JaxbType & {
    action?: Action;
    compression_format?: ArchiveFormat;
    name?: string;
});

