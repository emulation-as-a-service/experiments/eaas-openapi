/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SnapshotRequest } from './SnapshotRequest.ts';

export type SaveObjectEnvironmentRequest = (SnapshotRequest & {
    objectId?: string;
    title?: string;
    objectArchiveId?: string;
    driveId?: number;
});

