/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AbstractDataResource } from './AbstractDataResource.ts';
import type { AccessType } from './AccessType.ts';
import type { TransportType } from './TransportType.ts';

export type Binding = (AbstractDataResource & {
    url?: string;
    transport?: TransportType;
    access?: AccessType;
    localAlias?: string;
    filesize?: number;
    imageType?: string;
    username?: string;
    password?: string;
});

