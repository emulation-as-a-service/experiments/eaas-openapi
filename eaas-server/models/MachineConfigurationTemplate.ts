/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { MachineConfiguration } from './MachineConfiguration.ts';

export type MachineConfigurationTemplate = MachineConfiguration;

