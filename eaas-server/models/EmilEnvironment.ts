/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilEnvironmentOwner } from './EmilEnvironmentOwner.ts';
import type { EmilEnvironmentPermissions } from './EmilEnvironmentPermissions.ts';
import type { JaxbType } from './JaxbType.ts';
import type { NetworkingType } from './NetworkingType.ts';

export type EmilEnvironment = (JaxbType & {
    envId?: string;
    archive?: string;
    os?: string;
    title?: string;
    description?: string;
    version?: string;
    emulator?: string;
    timeContext?: string;
    author?: string;
    linuxRuntime?: boolean;
    enableRelativeMouse?: boolean;
    enablePrinting?: boolean;
    shutdownByOs?: boolean;
    owner?: EmilEnvironmentOwner;
    permissions?: EmilEnvironmentPermissions;
    canProcessAdditionalFiles?: boolean;
    xpraEncoding?: string;
    networking?: NetworkingType;
    helpText?: string;
    parentEnvId?: string;
    childrenEnvIds?: Array<string>;
    branches?: Array<string>;
    type?: string;
    timestamp?: string;
    deleted?: boolean;
});

