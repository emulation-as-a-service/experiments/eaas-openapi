/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DriveDataSource } from './DriveDataSource.ts';
import type { MediumType } from './MediumType.ts';

export type UserMedium = (DriveDataSource & {
    mediumType?: MediumType;
    url?: string;
    name?: string;
});

