/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentWithExternalFilesRequest } from './xml_ns0_componentWithExternalFilesRequest.ts';

export type xml_ns0_container = (xml_ns0_componentWithExternalFilesRequest & {
    archive?: string;
    environment?: string;
});

