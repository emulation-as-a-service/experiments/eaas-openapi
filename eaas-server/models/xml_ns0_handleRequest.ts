/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_handleRequest = {
    handle?: string;
    index?: number;
    value?: string;
};

