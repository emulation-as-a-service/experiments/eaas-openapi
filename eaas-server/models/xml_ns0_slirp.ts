/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentRequest } from './xml_ns0_componentRequest.ts';

export type xml_ns0_slirp = (xml_ns0_componentRequest & {
    dhcp?: boolean;
    dnsServer?: string;
    gateway?: string;
    hwAddress?: string;
    netmask?: string;
    network?: string;
});

