/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_operatingSystemInformation } from './xml_ns0_operatingSystemInformation.ts';

export type xml_ns0_operatingSystems = {
    operatingSystemInformations?: Array<xml_ns0_operatingSystemInformation>;
};

