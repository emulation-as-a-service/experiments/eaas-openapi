/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_softwarePackage } from './xml_ns1_softwarePackage.ts';

export type xml_ns0_eaasiSoftwareObject = {
    metsData?: string;
    softwarePackage?: xml_ns1_softwarePackage;
};

