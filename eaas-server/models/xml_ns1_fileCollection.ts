/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_file } from './xml_ns1_file.ts';

export type xml_ns1_fileCollection = {
    archive?: string;
    file?: Array<xml_ns1_file>;
    id?: string;
    label?: string;
};

