/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_objectType = {
    ID?: string;
    LABEL?: string;
    LOCTYPE?: string;
    OTHERLOCTYPE?: string;
    actuate?: string;
    arcrole?: string;
    href?: string;
    role?: string;
    show?: string;
    title?: string;
    type?: string;
};

