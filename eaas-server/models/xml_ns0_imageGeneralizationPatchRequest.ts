/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns5_imageType } from './xml_ns5_imageType.ts';

export type xml_ns0_imageGeneralizationPatchRequest = {
    archive?: string;
    imageId?: string;
    imageType?: xml_ns5_imageType;
};

