/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImageBuilderMetadata } from './ImageBuilderMetadata.ts';

export type DockerImport = (ImageBuilderMetadata & {
    entryProcesses?: Array<string>;
    tag?: string;
    layers?: Array<string>;
    emulatorVersion?: string;
    emulatorType?: string;
    workingDir?: string;
    imageRef?: string;
    digest?: string;
    envVariables?: Array<string>;
});

