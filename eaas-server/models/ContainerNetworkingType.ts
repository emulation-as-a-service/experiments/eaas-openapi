/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { NetworkingType } from './NetworkingType.ts';

export type ContainerNetworkingType = (NetworkingType & {
    isDHCPenabled?: boolean;
    isTelnetEnabled?: boolean;
});

