/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type RevertRevisionRequest = (JaxbType & {
    currentId?: string;
    revId?: string;
});

