/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_Agent = {
    ID?: string;
    ROLE?: string;
    OTHERROLE?: string;
    TYPE?: string;
    OTHERTYPE?: string;
    name?: string;
    note?: Array<string>;
};

