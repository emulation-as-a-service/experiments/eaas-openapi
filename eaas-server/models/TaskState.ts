/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type TaskState = (JaxbType & {
    taskId?: string;
    result?: string;
    failed?: boolean;
    done?: boolean;
});

