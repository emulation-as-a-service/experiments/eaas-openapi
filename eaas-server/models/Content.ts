/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Content = {
    type?: string;
    wikidata?: string;
};

