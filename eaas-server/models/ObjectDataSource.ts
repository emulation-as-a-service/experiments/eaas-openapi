/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DriveDataSource } from './DriveDataSource.ts';

export type ObjectDataSource = (DriveDataSource & {
    id?: string;
    archive?: string;
});

