/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilResponseType } from './EmilResponseType.ts';

export type UserInfoResponse = (EmilResponseType & {
    userId?: string;
    fullName?: string;
    role?: string;
    username?: string;
});

