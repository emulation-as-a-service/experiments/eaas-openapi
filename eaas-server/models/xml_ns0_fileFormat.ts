/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_fileFormat = {
    count?: number;
    fromDate?: number;
    name?: string;
    puid?: string;
    toDate?: number;
};

