/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';

export type xml_ns0_remoteExportRequest = (xml_ns0_emilRequestType & {
    envId?: Array<string>;
    exportObjectEmbedded?: boolean;
    objectArchiveHost?: string;
    wsHost?: string;
});

