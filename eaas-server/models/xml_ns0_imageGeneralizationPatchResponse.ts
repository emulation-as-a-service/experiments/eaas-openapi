/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_imageGeneralizationPatchResponse = {
    imageId?: string;
    status?: string;
};

