/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type LinuxRuntimeContainerReq = {
    userContainerEnvironment?: string;
    userContainerArchive?: string;
    isDHCPenabled: boolean;
    isTelnetEnabled: boolean;
    userEnvironment?: Array<string>;
};

