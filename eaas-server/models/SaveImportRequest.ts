/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SnapshotRequest } from './SnapshotRequest.ts';

export type SaveImportRequest = (SnapshotRequest & {
    title?: string;
});

