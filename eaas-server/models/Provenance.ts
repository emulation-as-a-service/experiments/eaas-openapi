/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Provenance = {
    ociSourceUrl?: string;
    versionTag?: string;
    layers?: Array<string>;
};

