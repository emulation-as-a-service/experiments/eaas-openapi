/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DiskType } from './DiskType.ts';
import type { EmilResponseType } from './EmilResponseType.ts';
import type { EnvironmentInfo } from './EnvironmentInfo.ts';
import type { IdentificationData } from './IdentificationData.ts';
import type { OperatingSystem } from './OperatingSystem.ts';

export type ClassificationResult = (EmilResponseType & {
    environmentList?: Array<EnvironmentInfo>;
    manualEnvironmentList?: Array<EnvironmentInfo>;
    suggested?: Array<OperatingSystem>;
    fileFormatMap?: Record<string, IdentificationData>;
    mediaFormats?: Record<string, DiskType>;
    objectId?: string;
    userDescription?: string;
});

