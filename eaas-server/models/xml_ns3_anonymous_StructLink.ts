/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_structLinkType } from './xml_ns3_structLinkType.ts';

export type xml_ns3_anonymous_StructLink = xml_ns3_structLinkType;

