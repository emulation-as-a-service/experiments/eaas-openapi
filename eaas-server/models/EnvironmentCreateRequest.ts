/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DriveSetting } from './DriveSetting.ts';
import type { JaxbType } from './JaxbType.ts';

export type EnvironmentCreateRequest = (JaxbType & {
    templateId?: string;
    label?: string;
    nativeConfig?: string;
    driveSettings?: Array<DriveSetting>;
    enablePrinting?: boolean;
    enableRelativeMouse?: boolean;
    useWebRTC?: boolean;
    useXpra?: boolean;
    xpraEncoding?: string;
    shutdownByOs?: boolean;
    operatingSystemId?: string;
    romId?: string;
    romLabel?: string;
    enableNetwork?: boolean;
    enableInternet?: boolean;
});

