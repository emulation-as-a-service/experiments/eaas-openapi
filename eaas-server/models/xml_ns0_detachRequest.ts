/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentTitleCreator } from './xml_ns0_componentTitleCreator.ts';

export type xml_ns0_detachRequest = {
    componentTitle?: xml_ns0_componentTitleCreator;
    lifetime?: number;
    sessionName?: string;
};

