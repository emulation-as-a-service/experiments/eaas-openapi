/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BaseFileSource } from './BaseFileSource.ts';

export type FileURL = (BaseFileSource & {
    url?: string;
});

