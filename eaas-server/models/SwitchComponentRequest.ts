/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentRequest } from './ComponentRequest.ts';
import type { NetworkSwitchConfiguration } from './NetworkSwitchConfiguration.ts';

export type SwitchComponentRequest = (ComponentRequest & {
    config?: NetworkSwitchConfiguration;
});

