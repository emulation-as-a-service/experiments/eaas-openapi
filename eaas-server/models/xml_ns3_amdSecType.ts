/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_mdSecType } from './xml_ns3_mdSecType.ts';

export type xml_ns3_amdSecType = {
    ID?: string;
    techMD?: Array<xml_ns3_mdSecType>;
    rightsMD?: Array<xml_ns3_mdSecType>;
    sourceMD?: Array<xml_ns3_mdSecType>;
    digiprovMD?: Array<xml_ns3_mdSecType>;
};

