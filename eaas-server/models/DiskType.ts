/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Content } from './Content.ts';
import type { JaxbType } from './JaxbType.ts';

export type DiskType = (JaxbType & {
    type?: string;
    localAlias?: string;
    content?: Array<Content>;
    size?: string;
    path?: string;
});

