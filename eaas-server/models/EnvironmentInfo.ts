/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type EnvironmentInfo = (JaxbType & {
    id?: string;
    label?: string;
    objectEnvironment?: boolean;
});

