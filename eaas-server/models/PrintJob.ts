/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type PrintJob = (JaxbType & {
    label?: string;
    dataHandler?: string;
});

