/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_Stream = {
    ADMID?: Array<string>;
    BEGIN?: string;
    BETYPE?: string;
    DMDID?: Array<string>;
    END?: string;
    ID?: string;
    OWNERID?: string;
    streamType?: string;
};

