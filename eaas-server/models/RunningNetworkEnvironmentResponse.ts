/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { Session } from './Session.ts';

export type RunningNetworkEnvironmentResponse = (JaxbType & {
    session?: Session;
    networkEnvId?: string;
});

