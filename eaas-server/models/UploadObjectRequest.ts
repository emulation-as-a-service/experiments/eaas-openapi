/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilRequestType } from './EmilRequestType.ts';

export type UploadObjectRequest = (EmilRequestType & {
    objectId?: string;
    archive?: string;
});

