/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_MdRef = {
    CHECKSUM?: string;
    CHECKSUMTYPE?: string;
    CREATED?: string;
    ID?: string;
    LABEL?: string;
    LOCTYPE?: string;
    MDTYPE?: string;
    MDTYPEVERSION?: string;
    MIMETYPE?: string;
    OTHERLOCTYPE?: string;
    OTHERMDTYPE?: string;
    SIZE?: number;
    XPTR?: string;
    actuate?: string;
    arcrole?: string;
    href?: string;
    role?: string;
    show?: string;
    title?: string;
    type?: string;
};

