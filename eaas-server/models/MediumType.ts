/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum MediumType {
    HDD = 'hdd',
    CDROM = 'cdrom',
    FLOPPY = 'floppy',
}
