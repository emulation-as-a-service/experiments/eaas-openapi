/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_containerImageMetadata } from './xml_ns0_containerImageMetadata.ts';

export type xml_ns0_createContainerImageResult = {
    containerUrl?: string;
    metadata?: xml_ns0_containerImageMetadata;
};

