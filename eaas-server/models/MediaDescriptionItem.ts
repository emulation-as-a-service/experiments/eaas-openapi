/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type MediaDescriptionItem = (JaxbType & {
    id?: string;
    label?: string;
});

