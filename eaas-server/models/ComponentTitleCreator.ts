/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type ComponentTitleCreator = (JaxbType & {
    componentName?: string;
    componentId?: string;
});

