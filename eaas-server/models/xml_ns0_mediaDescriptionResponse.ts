/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_classificationResult } from './xml_ns0_classificationResult.ts';
import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';
import type { xml_ns1_digitalObjectMetadata } from './xml_ns1_digitalObjectMetadata.ts';
import type { xml_ns1_fileCollection } from './xml_ns1_fileCollection.ts';

export type xml_ns0_mediaDescriptionResponse = (xml_ns0_emilResponseType & {
    mediaItems?: xml_ns1_fileCollection;
    metadata?: xml_ns1_digitalObjectMetadata;
    objectEnvironments?: xml_ns0_classificationResult;
});

