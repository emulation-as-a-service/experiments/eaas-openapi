/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type HandleListResponse = (JaxbType & {
    handles?: Array<string>;
});

