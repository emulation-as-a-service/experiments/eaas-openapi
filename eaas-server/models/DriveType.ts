/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum DriveType {
    CDROM = 'cdrom',
    DISK = 'disk',
    FLOPPY = 'floppy',
    CART = 'cart',
}
