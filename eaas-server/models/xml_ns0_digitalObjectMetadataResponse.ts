/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';

export type xml_ns0_digitalObjectMetadataResponse = (xml_ns0_emilResponseType & {
    customData?: any;
    description?: string;
    summary?: string;
    thumbnail?: string;
    title?: string;
    wikiDataId?: string;
});

