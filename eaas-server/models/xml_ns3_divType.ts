/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_Fptr } from './xml_ns3_anonymous_Fptr.ts';
import type { xml_ns3_anonymous_Mptr } from './xml_ns3_anonymous_Mptr.ts';

export type xml_ns3_divType = {
    ID?: string;
    ORDER?: number;
    ORDERLABEL?: string;
    LABEL?: string;
    DMDID?: Array<string>;
    ADMID?: Array<string>;
    TYPE?: string;
    CONTENTIDS?: Array<string>;
    label?: string;
    mptr?: Array<xml_ns3_anonymous_Mptr>;
    fptr?: Array<xml_ns3_anonymous_Fptr>;
    div?: Array<xml_ns3_divType>;
};

