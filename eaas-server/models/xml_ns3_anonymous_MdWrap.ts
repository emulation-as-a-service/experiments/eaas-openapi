/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_XmlData } from './xml_ns3_anonymous_XmlData.ts';

export type xml_ns3_anonymous_MdWrap = {
    ID?: string;
    LABEL?: string;
    MIMETYPE?: string;
    SIZE?: number;
    CREATED?: string;
    CHECKSUM?: string;
    CHECKSUMTYPE?: string;
    MDTYPE?: string;
    OTHERMDTYPE?: string;
    MDTYPEVERSION?: string;
    binData?: string;
    xmlData?: xml_ns3_anonymous_XmlData;
};

