/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_binding } from './xml_ns1_binding.ts';

export type xml_ns1_imageArchiveBinding = (xml_ns1_binding & {
    urlPrefix?: string;
    imageId?: string;
    type?: string;
    fileSystemType?: string;
    backendName?: string;
});

