/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_DHCPConfiguration } from './xml_ns0_DHCPConfiguration.ts';
import type { xml_ns0_environmentNetworkConfiguration } from './xml_ns0_environmentNetworkConfiguration.ts';

export type xml_ns0_networkConfiguration = {
    archived_internet_date?: string;
    dhcp?: xml_ns0_DHCPConfiguration;
    environments?: Array<xml_ns0_environmentNetworkConfiguration>;
    gateway?: string;
    network?: string;
    upstream_dns?: string;
};

