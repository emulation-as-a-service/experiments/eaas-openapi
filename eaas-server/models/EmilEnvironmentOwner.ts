/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EmilEnvironmentOwner = {
    usergroup?: string;
    username?: string;
};

