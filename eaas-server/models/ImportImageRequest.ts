/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type ImportImageRequest = (JaxbType & {
    url?: string;
    label?: string;
    imageType?: string;
});

