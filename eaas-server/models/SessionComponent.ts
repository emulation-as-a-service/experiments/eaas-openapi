/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { NetworkResponse } from './NetworkResponse.ts';

export type SessionComponent = (JaxbType & {
    componentId?: string;
    environmentId?: string;
    type?: string;
    networkLabel?: string;
    networkData?: NetworkResponse;
});

