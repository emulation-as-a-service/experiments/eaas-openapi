/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SnapshotRequest } from './SnapshotRequest.ts';

export type SaveDerivateRequest = (SnapshotRequest & {
    softwareId?: string;
});

