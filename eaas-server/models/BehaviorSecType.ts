/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BehaviorType } from './BehaviorType.ts';

export type BehaviorSecType = {
    behaviorSec?: Array<BehaviorSecType>;
    behavior?: Array<BehaviorType>;
    ID?: string;
    CREATED?: number;
    LABEL?: string;
};

