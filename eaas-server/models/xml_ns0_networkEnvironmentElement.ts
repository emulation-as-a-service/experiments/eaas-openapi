/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_networkEnvironmentElement = {
    envId?: string;
    fqdn?: string;
    label?: string;
    macAddress?: string;
    serverIp?: string;
    serverPorts?: Array<number>;
    title?: string;
    wildcard?: boolean;
};

