/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentConfiguration } from './ComponentConfiguration.ts';

export type VdeSlirpConfiguration = (ComponentConfiguration & {
    hwAddress?: string;
    dhcp?: boolean;
    gateway?: string;
    network?: string;
    dnsServer?: string;
    netmask?: string;
});

