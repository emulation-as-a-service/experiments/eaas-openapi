/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilEnvironmentOwner } from './xml_ns0_emilEnvironmentOwner.ts';
import type { xml_ns0_emilEnvironmentPermissions } from './xml_ns0_emilEnvironmentPermissions.ts';
import type { xml_ns0_networkingType } from './xml_ns0_networkingType.ts';

export type xml_ns0_emilEnvironment = {
    archive?: string;
    author?: string;
    branches?: Array<string>;
    canProcessAdditionalFiles?: boolean;
    childrenEnvIds?: Array<string>;
    deleted?: boolean;
    description?: string;
    emulator?: string;
    enablePrinting?: boolean;
    enableRelativeMouse?: boolean;
    envId?: string;
    helpText?: string;
    linuxRuntime?: boolean;
    networking?: xml_ns0_networkingType;
    os?: string;
    owner?: xml_ns0_emilEnvironmentOwner;
    parentEnvId?: string;
    permissions?: xml_ns0_emilEnvironmentPermissions;
    shutdownByOs?: boolean;
    timeContext?: string;
    timestamp?: string;
    title?: string;
    type?: string;
    version?: string;
    xpraEncoding?: string;
};

