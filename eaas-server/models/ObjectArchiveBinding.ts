/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AbstractDataResource } from './AbstractDataResource.ts';

export type ObjectArchiveBinding = (AbstractDataResource & {
    archiveHost?: string;
    objectId?: string;
    archive?: string;
});

