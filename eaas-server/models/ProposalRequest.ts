/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DataType } from './DataType.ts';

export type ProposalRequest = {
    data_url?: string;
    data_type?: DataType;
};

