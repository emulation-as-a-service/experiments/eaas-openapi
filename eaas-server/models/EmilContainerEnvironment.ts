/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerNetworkingType } from './ContainerNetworkingType.ts';
import type { EmilEnvironment } from './EmilEnvironment.ts';

export type EmilContainerEnvironment = (EmilEnvironment & {
    input?: string;
    output?: string;
    args?: Array<string>;
    env?: Array<string>;
    runtimeId?: string;
    serviceContainer?: boolean;
    networking?: ContainerNetworkingType;
    digest?: string;
});

