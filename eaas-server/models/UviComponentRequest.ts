/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { MachineComponentRequest } from './MachineComponentRequest.ts';
import type { UviFile } from './UviFile.ts';

export type UviComponentRequest = (MachineComponentRequest & {
    uviUrl?: string;
    uviFilename?: string;
    uviWriteable?: boolean;
    auxFiles?: Array<UviFile>;
});

