/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum FileSystemType {
    RAW = 'raw',
    FAT16 = 'fat16',
    FAT32 = 'fat32',
    VFAT = 'vfat',
    NTFS = 'ntfs',
    EXT2 = 'ext2',
    EXT3 = 'ext3',
    EXT4 = 'ext4',
    HFS = 'hfs',
    ISO9660 = 'iso9660',
}
