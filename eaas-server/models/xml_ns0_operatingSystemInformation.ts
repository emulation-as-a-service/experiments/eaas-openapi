/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_operatingSystemInformation = {
    extensions?: Array<string>;
    id?: string;
    label?: string;
    puids?: Array<string>;
};

