/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_Mptr = {
    CONTENTIDS?: Array<string>;
    ID?: string;
    LOCTYPE?: string;
    OTHERLOCTYPE?: string;
    actuate?: string;
    arcrole?: string;
    href?: string;
    role?: string;
    show?: string;
    title?: string;
    type?: string;
};

