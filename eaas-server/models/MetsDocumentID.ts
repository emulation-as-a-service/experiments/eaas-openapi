/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type MetsDocumentID = {
    value?: string;
    ID?: string;
    TYPE?: string;
};

