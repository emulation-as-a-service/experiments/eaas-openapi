/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_objectType } from './xml_ns3_objectType.ts';

export type xml_ns3_behaviorType = {
    ID?: string;
    STRUCTID?: Array<string>;
    BTYPE?: string;
    CREATED?: string;
    LABEL?: string;
    GROUPID?: string;
    ADMID?: Array<string>;
    interfaceDef?: xml_ns3_objectType;
    mechanism?: xml_ns3_objectType;
};

