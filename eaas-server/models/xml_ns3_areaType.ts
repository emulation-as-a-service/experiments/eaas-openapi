/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_areaType = {
    ADMID?: Array<string>;
    BEGIN?: string;
    BETYPE?: string;
    CONTENTIDS?: Array<string>;
    COORDS?: string;
    END?: string;
    EXTENT?: string;
    EXTTYPE?: string;
    FILEID?: string;
    ID?: string;
    SHAPE?: string;
};

