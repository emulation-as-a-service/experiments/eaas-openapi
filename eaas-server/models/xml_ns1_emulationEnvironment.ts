/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_anonymous_NativeConfig } from './xml_ns0_anonymous_NativeConfig.ts';
import type { xml_ns1_binding } from './xml_ns1_binding.ts';
import type { xml_ns1_drive } from './xml_ns1_drive.ts';
import type { xml_ns1_emulatorSpec } from './xml_ns1_emulatorSpec.ts';
import type { xml_ns1_environment } from './xml_ns1_environment.ts';
import type { xml_ns1_nic } from './xml_ns1_nic.ts';
import type { xml_ns1_objectArchiveBinding } from './xml_ns1_objectArchiveBinding.ts';
import type { xml_ns1_uiOptions } from './xml_ns1_uiOptions.ts';

export type xml_ns1_emulationEnvironment = (xml_ns1_environment & {
    arch?: string;
    model?: string;
    emulator?: xml_ns1_emulatorSpec;
    ui_options?: xml_ns1_uiOptions;
    checkpointBindingId?: string;
    operatingSystemId?: string;
    installedSoftwareId?: Array<string>;
    drive?: Array<xml_ns1_drive>;
    nic?: Array<xml_ns1_nic>;
    binding?: xml_ns1_binding;
    objectArchiveBinding?: xml_ns1_objectArchiveBinding;
    nativeConfig?: xml_ns0_anonymous_NativeConfig;
    outputBindingId?: string;
    isLinuxRuntime?: boolean;
});

