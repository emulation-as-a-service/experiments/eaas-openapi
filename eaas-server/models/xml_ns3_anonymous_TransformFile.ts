/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_TransformFile = {
    ID?: string;
    TRANSFORMALGORITHM?: string;
    TRANSFORMBEHAVIOR?: string;
    TRANSFORMKEY?: string;
    TRANSFORMORDER?: number;
    TRANSFORMTYPE?: string;
};

