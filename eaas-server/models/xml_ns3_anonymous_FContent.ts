/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_XmlData } from './xml_ns3_anonymous_XmlData.ts';

export type xml_ns3_anonymous_FContent = {
    ID?: string;
    USE?: string;
    binData?: string;
    xmlData?: xml_ns3_anonymous_XmlData;
};

