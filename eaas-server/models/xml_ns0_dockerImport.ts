/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_imageBuilderMetadata } from './xml_ns0_imageBuilderMetadata.ts';

export type xml_ns0_dockerImport = (xml_ns0_imageBuilderMetadata & {
    digest?: string;
    emulatorType?: string;
    emulatorVersion?: string;
    entryProcesses?: Array<string>;
    envVariables?: Array<string>;
    imageRef?: string;
    layers?: Array<string>;
    tag?: string;
    workingDir?: string;
});

