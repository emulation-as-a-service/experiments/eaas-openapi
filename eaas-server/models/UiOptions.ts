/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Html5Options } from './Html5Options.ts';
import type { InputOptions } from './InputOptions.ts';
import type { TimeOptions } from './TimeOptions.ts';

export type UiOptions = {
    audio_system?: string;
    forwarding_system?: string;
    html5?: Html5Options;
    time?: TimeOptions;
    input?: InputOptions;
};

