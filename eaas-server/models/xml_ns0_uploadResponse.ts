/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';
import type { xml_ns0_uploadedItem } from './xml_ns0_uploadedItem.ts';

export type xml_ns0_uploadResponse = (xml_ns0_emilResponseType & {
    uploadedItemList?: Array<xml_ns0_uploadedItem>;
    uploads?: Array<string>;
});

