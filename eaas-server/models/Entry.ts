/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Alias } from './Alias.ts';

export type Entry = {
    key?: string;
    value?: Alias;
};

