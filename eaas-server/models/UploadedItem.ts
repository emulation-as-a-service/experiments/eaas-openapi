/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UploadedItem = {
    url?: string;
    filename?: string;
};

