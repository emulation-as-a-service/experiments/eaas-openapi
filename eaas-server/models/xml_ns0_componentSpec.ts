/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_componentSpec = {
    componentId?: string;
    environmentId?: string;
    saveEnvironmentLabel?: string;
    shouldSaveEnvironment?: boolean;
};

