/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { NetworkingType } from './NetworkingType.ts';

export type EmilRequestType = (JaxbType & {
    userId?: string;
    connectEnvs?: boolean;
    networking?: NetworkingType;
});

