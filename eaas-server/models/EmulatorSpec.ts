/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Architecture } from './Architecture.ts';
import type { Machine } from './Machine.ts';

export type EmulatorSpec = {
    machine?: Machine;
    architecture?: Architecture;
    bean?: string;
    version?: string;
    upstreamDigest?: string;
    ociSourceUrl?: string;
    containerName?: string;
    containerVersion?: string;
};

