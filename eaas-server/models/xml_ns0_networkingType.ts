/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_networkingType = {
    connectEnvs?: boolean;
    enableInternet?: boolean;
    enableSocks?: boolean;
    gwPrivateIp?: string;
    gwPrivateMask?: string;
    helpText?: string;
    localServerMode?: boolean;
    serverIp?: string;
    serverMode?: boolean;
    serverPort?: string;
};

