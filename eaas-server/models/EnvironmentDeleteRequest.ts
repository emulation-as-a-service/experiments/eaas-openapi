/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EnvironmentDeleteRequest = {
    envId?: string;
    deleteMetaData: boolean;
    deleteImage: boolean;
    force: boolean;
};

