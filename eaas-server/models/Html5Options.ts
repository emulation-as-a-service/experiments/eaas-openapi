/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Html5Options = {
    pointer_lock: boolean;
    crt?: string;
};

