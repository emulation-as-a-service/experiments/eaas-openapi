/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentWithExternalFilesRequest } from './ComponentWithExternalFilesRequest.ts';
import type { Drive } from './Drive.ts';
import type { LinuxRuntimeContainerReq } from './LinuxRuntimeContainerReq.ts';
import type { UserMedium } from './UserMedium.ts';

export type MachineComponentRequest = (ComponentWithExternalFilesRequest & {
    environment?: string;
    keyboardLayout?: string;
    keyboardModel?: string;
    object?: string;
    archive?: string;
    objectArchive?: string;
    software?: string;
    lockEnvironment?: boolean;
    emulatorVersion?: string;
    nic?: string;
    headless?: boolean;
    sessionLifetime?: number;
    linuxRuntimeData?: LinuxRuntimeContainerReq;
    hasOutput?: boolean;
    outputDriveId?: string;
    userMedia?: Array<UserMedium>;
    drives?: Array<Drive>;
});

