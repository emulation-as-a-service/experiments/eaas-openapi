/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilRequestType } from './EmilRequestType.ts';

export type ReplicateImagesRequest = (EmilRequestType & {
    replicateList?: Array<string>;
    destArchive?: string;
});

