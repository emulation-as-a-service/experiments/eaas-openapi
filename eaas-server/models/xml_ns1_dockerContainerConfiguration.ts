/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_containerConfiguration } from './xml_ns1_containerConfiguration.ts';

export type xml_ns1_dockerContainerConfiguration = (xml_ns1_containerConfiguration & {
    image?: string;
});

