/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Binding } from './Binding.ts';

export type ImageArchiveBinding = (Binding & {
    urlPrefix?: string;
    imageId?: string;
    type?: string;
    fileSystemType?: string;
    backendName?: string;
});

