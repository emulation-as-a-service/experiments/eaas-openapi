/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Entry } from './Entry.ts';

export type Aliases = {
    entry?: Array<Entry>;
};

