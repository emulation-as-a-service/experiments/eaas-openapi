/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_exportRequest = {
    archive?: string;
    deleteAfterExport?: boolean;
    envId?: string;
    standalone?: boolean;
};

