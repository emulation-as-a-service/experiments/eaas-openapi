/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilResponseType } from './EmilResponseType.ts';

export type ReplicateImagesResponse = (EmilResponseType & {
    taskList?: Array<string>;
});

