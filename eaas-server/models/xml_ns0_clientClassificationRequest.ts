/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_clientClassificationRequest = {
    archiveId?: string;
    filename?: string;
    noUpdate?: boolean;
    objectId?: string;
    updateClassification?: boolean;
    updateProposal?: boolean;
    url?: string;
};

