/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns5_imageType {
    BASE = 'base',
    OBJECT = 'object',
    USER = 'user',
    DERIVATE = 'derivate',
    SYSTEM = 'system',
    TEMPLATE = 'template',
    TMP = 'tmp',
    SESSIONS = 'sessions',
    ROMS = 'roms',
    CONTAINERS = 'containers',
    CHECKPOINTS = 'checkpoints',
    RUNTIME = 'runtime',
}
