/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentRequest } from './xml_ns0_componentRequest.ts';
import type { xml_ns1_nodeTcpConfiguration } from './xml_ns1_nodeTcpConfiguration.ts';

export type xml_ns0_nodeTcpComponentRequest = (xml_ns0_componentRequest & {
    config?: xml_ns1_nodeTcpConfiguration;
});

