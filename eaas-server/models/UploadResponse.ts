/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilResponseType } from './EmilResponseType.ts';
import type { UploadedItem } from './UploadedItem.ts';

export type UploadResponse = (EmilResponseType & {
    uploads?: Array<string>;
    uploadedItemList?: Array<UploadedItem>;
});

