/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { NetworkEnvironmentElement } from './NetworkEnvironmentElement.ts';
import type { NetworkEnvironmentNetworkingType } from './NetworkEnvironmentNetworkingType.ts';

export type NetworkEnvironment = (JaxbType & {
    emilEnvironments?: Array<NetworkEnvironmentElement>;
    title?: string;
    envId?: string;
    description?: string;
    network?: string;
    gateway?: string;
    upstream_dns?: string;
    dnsServiceEnvId?: string;
    smbServiceEnvId?: string;
    linuxArchiveProxyEnvId?: string;
    startupEnvId?: string;
    networking?: NetworkEnvironmentNetworkingType;
    type?: string;
});

