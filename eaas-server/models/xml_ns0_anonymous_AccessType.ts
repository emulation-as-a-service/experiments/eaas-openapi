/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns0_anonymous_AccessType {
    COW = 'cow',
    COPY = 'copy',
}
