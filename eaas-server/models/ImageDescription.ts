/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ImageDescription = {
    url?: string;
    id?: string;
    type?: string;
    fstype?: string;
};

