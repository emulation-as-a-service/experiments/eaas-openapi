/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProposalResponse = {
    id?: string;
    message?: string;
};

