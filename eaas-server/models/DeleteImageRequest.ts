/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DeleteImageRequest = {
    imageId?: string;
    imageArchive?: string;
};

