/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_abstractDataResource } from './xml_ns1_abstractDataResource.ts';

export type xml_ns1_objectArchiveBinding = (xml_ns1_abstractDataResource & {
    archiveHost?: string;
    objectId?: string;
    archive?: string;
});

