/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_snapshotRequest } from './xml_ns0_snapshotRequest.ts';

export type xml_ns0_saveUserSession = (xml_ns0_snapshotRequest & {
    archiveId?: string;
    envId?: string;
    objectId?: string;
});

