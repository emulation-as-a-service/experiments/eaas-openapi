/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type HandleValueResponse = {
    values?: Array<string>;
};

