/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_areaType } from './xml_ns3_areaType.ts';
import type { xml_ns3_parType } from './xml_ns3_parType.ts';
import type { xml_ns3_seqType } from './xml_ns3_seqType.ts';

export type xml_ns3_anonymous_Fptr = {
    ID?: string;
    FILEID?: string;
    CONTENTIDS?: Array<string>;
    par?: xml_ns3_parType;
    seq?: xml_ns3_seqType;
    area?: xml_ns3_areaType;
};

