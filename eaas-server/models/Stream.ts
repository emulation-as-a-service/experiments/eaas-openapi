/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Stream = {
    END?: string;
    DMDID?: Array<any>;
    BETYPE?: string;
    BEGIN?: string;
    streamType?: string;
    ADMID?: Array<any>;
    ID?: string;
    OWNERID?: string;
};

