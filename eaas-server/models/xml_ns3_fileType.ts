/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_FContent } from './xml_ns3_anonymous_FContent.ts';
import type { xml_ns3_anonymous_FLocat } from './xml_ns3_anonymous_FLocat.ts';
import type { xml_ns3_anonymous_Stream } from './xml_ns3_anonymous_Stream.ts';
import type { xml_ns3_anonymous_TransformFile } from './xml_ns3_anonymous_TransformFile.ts';

export type xml_ns3_fileType = {
    ID?: string;
    SEQ?: number;
    OWNERID?: string;
    ADMID?: Array<string>;
    DMDID?: Array<string>;
    GROUPID?: string;
    USE?: string;
    BEGIN?: string;
    END?: string;
    BETYPE?: string;
    MIMETYPE?: string;
    SIZE?: number;
    CREATED?: string;
    CHECKSUM?: string;
    CHECKSUMTYPE?: string;
    FLocat?: Array<xml_ns3_anonymous_FLocat>;
    FContent?: xml_ns3_anonymous_FContent;
    stream?: Array<xml_ns3_anonymous_Stream>;
    transformFile?: Array<xml_ns3_anonymous_TransformFile>;
    file?: Array<xml_ns3_fileType>;
};

