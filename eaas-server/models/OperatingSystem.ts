/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EnvironmentInfo } from './EnvironmentInfo.ts';

export type OperatingSystem = {
    id?: string;
    label?: string;
    defaultEnvironment?: EnvironmentInfo;
};

