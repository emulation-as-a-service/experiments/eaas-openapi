/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type NetworkResponse = (JaxbType & {
    id?: string;
    isLocalMode?: boolean;
    networkUrls?: Record<string, string>;
});

