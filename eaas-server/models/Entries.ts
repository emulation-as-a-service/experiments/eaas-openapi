/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Entry_Entries } from './Entry_Entries.ts';

export type Entries = {
    entry?: Array<Entry_Entries>;
};

