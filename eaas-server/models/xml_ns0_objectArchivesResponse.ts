/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';

export type xml_ns0_objectArchivesResponse = (xml_ns0_emilResponseType & {
    archives?: Array<string>;
});

