/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilResponseType } from './EmilResponseType.ts';

/**
 * SaveContainerResponse: respond with status and possible id of saved image
 */
export type SaveContainerResponse = (EmilResponseType & {
    id?: string;
});

