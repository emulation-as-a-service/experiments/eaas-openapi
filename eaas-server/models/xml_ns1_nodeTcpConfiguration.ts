/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentConfiguration } from './xml_ns0_componentConfiguration.ts';

export type xml_ns1_nodeTcpConfiguration = (xml_ns0_componentConfiguration & {
    destIp?: string;
    destPort?: string;
    dhcp?: boolean;
    dhcpNetworkAddress?: string;
    dhcpNetworkMask?: string;
    hwAddress?: string;
    socksMode?: boolean;
    socksPasswd?: string;
    socksUser?: string;
});

