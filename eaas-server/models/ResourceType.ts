/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum ResourceType {
    ISO = 'iso',
    DISK = 'disk',
    FLOPPY = 'floppy',
    CART = 'cart',
    ZIP = 'zip',
    TAR = 'tar',
    FILE = 'file',
}
