/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_linuxRuntimeContainerReq = {
    isDHCPenabled?: boolean;
    isTelnetEnabled?: boolean;
    userContainerArchive?: string;
    userContainerEnvironment?: string;
    userEnvironment?: Array<string>;
};

