/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns1_timeOptions = {
    offset?: string;
    epoch?: string;
};

