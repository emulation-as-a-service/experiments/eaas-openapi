/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_computeResult } from './xml_ns0_computeResult.ts';

export type xml_ns0_computeResponse = {
    id?: string;
    result?: Array<xml_ns0_computeResult>;
};

