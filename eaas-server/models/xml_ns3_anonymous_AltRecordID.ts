/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_AltRecordID = {
    ID?: string;
    TYPE?: string;
    '(value)'?: string;
};

