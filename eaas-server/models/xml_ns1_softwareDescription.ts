/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns1_softwareDescription = {
    archiveId?: string;
    isOperatingSystem?: boolean;
    isPublic?: boolean;
    label?: string;
    softwareId?: string;
};

