/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ComputeResult = {
    componentId?: string;
    state?: string;
    environmentId?: string;
    resultBlob?: string;
};

