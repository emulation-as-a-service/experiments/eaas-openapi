/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentWithExternalFilesRequest } from './ComponentWithExternalFilesRequest.ts';

export type ContainerComponentRequest = (ComponentWithExternalFilesRequest & {
    environment?: string;
    archive?: string;
});

