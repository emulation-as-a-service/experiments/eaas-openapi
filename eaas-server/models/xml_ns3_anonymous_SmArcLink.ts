/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_SmArcLink = {
    ADMID?: Array<string>;
    ARCTYPE?: string;
    ID?: string;
    actuate?: string;
    arcrole?: string;
    from?: string;
    show?: string;
    title?: string;
    to?: string;
    type?: string;
};

