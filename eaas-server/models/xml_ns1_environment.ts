/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentConfiguration } from './xml_ns0_componentConfiguration.ts';
import type { xml_ns1_environmentDescription } from './xml_ns1_environmentDescription.ts';

export type xml_ns1_environment = (xml_ns0_componentConfiguration & {
    id?: string;
    timestamp?: string;
    description?: xml_ns1_environmentDescription;
    metaDataVersion?: string;
    userTag?: string;
    configurationType?: string;
    deleted?: boolean;
});

