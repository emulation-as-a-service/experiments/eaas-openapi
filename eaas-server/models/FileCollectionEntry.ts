/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Binding } from './Binding.ts';
import type { DriveType } from './DriveType.ts';
import type { ResourceType } from './ResourceType.ts';

export type FileCollectionEntry = (Binding & {
    archive?: string;
    label?: string;
    order?: string;
    objectId?: string;
    isDefault?: boolean;
    type?: DriveType;
    resourceType?: ResourceType;
});

