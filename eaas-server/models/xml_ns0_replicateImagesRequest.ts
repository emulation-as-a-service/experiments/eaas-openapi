/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';

export type xml_ns0_replicateImagesRequest = (xml_ns0_emilRequestType & {
    destArchive?: string;
    replicateList?: Array<string>;
});

