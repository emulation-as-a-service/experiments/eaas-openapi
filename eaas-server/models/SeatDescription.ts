/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type SeatDescription = (JaxbType & {
    resource?: string;
    seats?: number;
});

