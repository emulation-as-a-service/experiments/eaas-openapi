/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_snapshotRequest } from './xml_ns0_snapshotRequest.ts';

export type xml_ns0_objectEnvironment = (xml_ns0_snapshotRequest & {
    driveId?: number;
    objectArchiveId?: string;
    objectId?: string;
    title?: string;
});

