/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { RuntimeListItem } from './RuntimeListItem.ts';

export type RuntimeListResponse = (JaxbType & {
    status?: string;
    runtimes?: Array<RuntimeListItem>;
});

