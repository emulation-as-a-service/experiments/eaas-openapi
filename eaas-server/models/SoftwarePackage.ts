/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type SoftwarePackage = (JaxbType & {
    documentation?: string;
    timestamp?: string;
    archive?: string;
    infoSource?: string;
    name?: string;
    supportedFileFormat?: Array<string>;
    releaseDate?: number;
    QID?: string;
    isPublic?: boolean;
    numSeats?: number;
    licence?: string;
    isOperatingSystem?: boolean;
    location?: string;
    description?: string;
    deleted?: boolean;
    objectId?: string;
    language?: string;
});

