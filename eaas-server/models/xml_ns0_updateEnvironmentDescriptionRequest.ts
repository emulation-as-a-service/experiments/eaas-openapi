/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_driveSetting } from './xml_ns0_driveSetting.ts';
import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';
import type { xml_ns1_drive } from './xml_ns1_drive.ts';

export type xml_ns0_updateEnvironmentDescriptionRequest = (xml_ns0_emilRequestType & {
    author?: string;
    containerEmulatorName?: string;
    containerEmulatorVersion?: string;
    description?: string;
    driveSettings?: Array<xml_ns0_driveSetting>;
    drives?: Array<xml_ns1_drive>;
    enablePrinting?: boolean;
    enableRelativeMouse?: boolean;
    envId?: string;
    helpText?: string;
    linuxRuntime?: boolean;
    nativeConfig?: string;
    os?: string;
    shutdownByOs?: boolean;
    time?: string;
    title?: string;
    useWebRTC?: boolean;
    useXpra?: boolean;
    userTag?: string;
    xpraEncoding?: string;
});

