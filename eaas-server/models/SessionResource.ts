/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { Type } from './Type.ts';

export type SessionResource = (JaxbType & {
    id?: string;
    type?: Type;
    keepalive_url?: string;
});

