/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_device } from './xml_ns1_device.ts';

export type xml_ns1_nic = (xml_ns1_device & {
    hwaddress?: string;
});

