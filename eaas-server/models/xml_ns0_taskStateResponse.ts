/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';

export type xml_ns0_taskStateResponse = (xml_ns0_emilResponseType & {
    isDone?: boolean;
    object?: string;
    taskId?: string;
    userData?: any;
});

