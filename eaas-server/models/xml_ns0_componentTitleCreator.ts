/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_componentTitleCreator = {
    componentId?: string;
    componentName?: string;
};

