/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentTitleCreator } from './ComponentTitleCreator.ts';
import type { JaxbType } from './JaxbType.ts';

export type DetachRequest = (JaxbType & {
    lifetime?: number;
    componentTitle?: ComponentTitleCreator;
    sessionName?: string;
});

