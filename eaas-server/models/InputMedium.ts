/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FileSystemType } from './FileSystemType.ts';
import type { FileURL } from './FileURL.ts';
import type { JaxbType } from './JaxbType.ts';
import type { MediumType } from './MediumType.ts';
import type { PartitionTableType } from './PartitionTableType.ts';

export type InputMedium = (JaxbType & {
    type?: MediumType;
    partition_table_type?: PartitionTableType;
    filesystem_type?: FileSystemType;
    size_mb?: number;
    destination?: string;
    content?: Array<FileURL>;
});

