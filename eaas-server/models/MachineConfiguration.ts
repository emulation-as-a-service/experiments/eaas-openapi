/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Binding } from './Binding.ts';
import type { Drive_api } from './Drive_api.ts';
import type { EmulatorSpec } from './EmulatorSpec.ts';
import type { Environment } from './Environment.ts';
import type { NativeConfig } from './NativeConfig.ts';
import type { Nic } from './Nic.ts';
import type { ObjectArchiveBinding } from './ObjectArchiveBinding.ts';
import type { UiOptions } from './UiOptions.ts';

export type MachineConfiguration = (Environment & {
    arch?: string;
    model?: string;
    emulator?: EmulatorSpec;
    ui_options?: UiOptions;
    checkpointBindingId?: string;
    operatingSystemId?: string;
    installedSoftwareId?: Array<string>;
    drive?: Array<Drive_api>;
    nic?: Array<Nic>;
    binding?: Array<Binding>;
    objectArchiveBinding?: Array<ObjectArchiveBinding>;
    nativeConfig?: NativeConfig;
    outputBindingId?: string;
    isLinuxRuntime?: boolean;
});

