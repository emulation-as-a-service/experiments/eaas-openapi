/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum ArchiveFormat {
    ZIP = 'zip',
    TAR = 'tar',
    SIMG = 'simg',
    DOCKER = 'docker',
}
