/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_tcpGatewayConfig } from './xml_ns0_tcpGatewayConfig.ts';

export type xml_ns0_networkRequest = {
    components?: Array<any>;
    dhcpNetworkAddress?: string;
    dhcpNetworkMask?: string;
    enableDhcp?: boolean;
    gateway?: string;
    hasInternet?: boolean;
    hasTcpGateway?: boolean;
    network?: string;
    networkEnvironmentId?: string;
    tcpGatewayConfig?: xml_ns0_tcpGatewayConfig;
};

