/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Drive_api } from './Drive_api.ts';
import type { DriveSetting } from './DriveSetting.ts';
import type { EmilRequestType } from './EmilRequestType.ts';

export type UpdateEnvironmentDescriptionRequest = (EmilRequestType & {
    linuxRuntime?: boolean;
    envId?: string;
    title?: string;
    description?: string;
    helpText?: string;
    time?: string;
    enablePrinting?: boolean;
    enableRelativeMouse?: boolean;
    shutdownByOs?: boolean;
    userTag?: string;
    os?: string;
    nativeConfig?: string;
    useXpra?: boolean;
    xpraEncoding?: string;
    containerEmulatorVersion?: string;
    containerEmulatorName?: string;
    author?: string;
    drives?: Array<Drive_api>;
    useWebRTC?: boolean;
    driveSettings?: Array<DriveSetting>;
});

