/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FileType } from './FileType.ts';

export type FileGrpType = {
    fileGrp?: Array<FileGrpType>;
    file?: Array<FileType>;
    ID?: string;
    USE?: string;
    ADMID?: Array<any>;
    VERSDATE?: number;
};

