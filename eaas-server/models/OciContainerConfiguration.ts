/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerConfiguration } from './ContainerConfiguration.ts';
import type { Process } from './Process.ts';

export type OciContainerConfiguration = (ContainerConfiguration & {
    process?: Process;
    rootfs?: string;
    isGui?: boolean;
    customSubdir?: string;
});

