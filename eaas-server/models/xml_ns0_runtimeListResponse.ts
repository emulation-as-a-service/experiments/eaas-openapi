/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_runtimeListItem } from './xml_ns0_runtimeListItem.ts';

export type xml_ns0_runtimeListResponse = {
    runtimes?: Array<xml_ns0_runtimeListItem>;
    status?: string;
};

