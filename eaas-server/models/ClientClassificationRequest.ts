/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type ClientClassificationRequest = (JaxbType & {
    archiveId?: string;
    objectId?: string;
    updateClassification?: boolean;
    updateProposal?: boolean;
    noUpdate?: boolean;
    url?: string;
    filename?: string;
});

