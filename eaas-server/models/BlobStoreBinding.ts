/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Binding } from './Binding.ts';
import type { FileSystemType } from './FileSystemType.ts';
import type { ResourceType } from './ResourceType.ts';

export type BlobStoreBinding = (Binding & {
    fileSystemType?: FileSystemType;
    offset?: number;
    resourceType?: ResourceType;
    mountFS?: boolean;
});

