/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilRequestType } from './EmilRequestType.ts';

export type SyncObjectRequest = (EmilRequestType & {
    archive?: string;
    objectIDs?: Array<string>;
});

