/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Binding } from './Binding.ts';
import type { Environment } from './Environment.ts';
import type { Input } from './Input.ts';
import type { ObjectArchiveBinding } from './ObjectArchiveBinding.ts';

export type ContainerConfiguration = (Environment & {
    inputs?: Array<Input>;
    output?: string;
    input?: string;
    binding?: Array<Binding>;
    objectArchiveBinding?: Array<ObjectArchiveBinding>;
    digest?: string;
});

