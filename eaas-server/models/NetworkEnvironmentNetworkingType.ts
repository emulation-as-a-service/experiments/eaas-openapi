/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerNetworkingType } from './ContainerNetworkingType.ts';

export type NetworkEnvironmentNetworkingType = (ContainerNetworkingType & {
    isArchivedInternetEnabled?: boolean;
    archiveInternetDate?: string;
    allowExternalConnections?: boolean;
    dhcpNetworkAddress?: string;
    dhcpNetworkMask?: string;
});

