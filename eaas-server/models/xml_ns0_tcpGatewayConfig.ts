/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_tcpGatewayConfig = {
    serverIp?: string;
    serverPort?: string;
    socks?: boolean;
};

