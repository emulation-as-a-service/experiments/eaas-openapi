/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum ImageType {
    BASE = 'base',
    OBJECT = 'object',
    USER = 'user',
    DERIVATE = 'derivate',
    SYSTEM = 'system',
    TEMPLATE = 'template',
    TMP = 'tmp',
    SESSIONS = 'sessions',
    ROMS = 'roms',
    CONTAINERS = 'containers',
    CHECKPOINTS = 'checkpoints',
    RUNTIME = 'runtime',
}
