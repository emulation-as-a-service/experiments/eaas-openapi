/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type FileFormat = (JaxbType & {
    puid?: string;
    name?: string;
    count?: number;
    fromDate?: number;
    toDate?: number;
});

