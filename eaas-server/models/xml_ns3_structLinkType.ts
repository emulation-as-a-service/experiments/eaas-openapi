/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_SmLink } from './xml_ns3_anonymous_SmLink.ts';
import type { xml_ns3_anonymous_SmLinkGrp } from './xml_ns3_anonymous_SmLinkGrp.ts';

export type xml_ns3_structLinkType = {
    ID?: string;
    smLink?: xml_ns3_anonymous_SmLink;
    smLinkGrp?: xml_ns3_anonymous_SmLinkGrp;
};

