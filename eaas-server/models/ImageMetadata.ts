/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImageDescription } from './ImageDescription.ts';
import type { Provenance } from './Provenance.ts';

export type ImageMetadata = {
    name?: string;
    version?: string;
    image?: ImageDescription;
    provenance?: Provenance;
    digest?: string;
    label?: string;
};

