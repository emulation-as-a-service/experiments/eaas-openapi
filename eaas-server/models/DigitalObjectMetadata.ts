/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type DigitalObjectMetadata = (JaxbType & {
    title?: string;
    summary?: string;
    metsData?: string;
    id?: string;
    thumbnail?: string;
    description?: string;
    customData?: Record<string, string>;
    wikiDataId?: string;
});

