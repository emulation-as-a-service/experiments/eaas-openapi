/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_containerNetworkingType } from './xml_ns0_containerNetworkingType.ts';
import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';

export type xml_ns0_updateContainerRequest = (xml_ns0_emilRequestType & {
    author?: string;
    containerRuntimeId?: string;
    description?: string;
    id?: string;
    inputFolder?: string;
    networking?: xml_ns0_containerNetworkingType;
    outputFolder?: string;
    processArgs?: Array<string>;
    processEnvs?: Array<string>;
    title?: string;
});

