/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_emilEnvironmentPermissions = {
    group?: string;
    user?: string;
};

