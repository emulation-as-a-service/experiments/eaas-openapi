/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImportFileInfo } from './ImportFileInfo.ts';

export type ImportObjectRequest = {
    label?: string;
    objectArchive?: string;
    files?: Array<ImportFileInfo>;
};

