/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EnvironmentNetworkConfiguration = {
    mac?: string;
    ip?: string;
    wildcard: boolean;
    hostnames?: Array<string>;
};

