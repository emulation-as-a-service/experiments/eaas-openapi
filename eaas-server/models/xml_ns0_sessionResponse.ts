/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_networkRequest } from './xml_ns0_networkRequest.ts';
import type { xml_ns0_sessionComponent } from './xml_ns0_sessionComponent.ts';

export type xml_ns0_sessionResponse = {
    components?: Array<xml_ns0_sessionComponent>;
    network?: xml_ns0_networkRequest;
};

