/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type ExportRequest = (JaxbType & {
    envId?: string;
    archive?: string;
    standalone?: boolean;
    deleteAfterExport?: boolean;
});

