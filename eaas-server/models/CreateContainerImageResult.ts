/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerImageMetadata } from './ContainerImageMetadata.ts';
import type { JaxbType } from './JaxbType.ts';

export type CreateContainerImageResult = (JaxbType & {
    containerUrl?: string;
    metadata?: ContainerImageMetadata;
});

