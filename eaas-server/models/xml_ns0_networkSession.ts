/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_session } from './xml_ns0_session.ts';

export type xml_ns0_networkSession = xml_ns0_session;

