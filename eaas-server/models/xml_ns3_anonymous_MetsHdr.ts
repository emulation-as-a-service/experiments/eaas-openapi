/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_Agent } from './xml_ns3_anonymous_Agent.ts';
import type { xml_ns3_anonymous_AltRecordID } from './xml_ns3_anonymous_AltRecordID.ts';
import type { xml_ns3_anonymous_MetsDocumentID } from './xml_ns3_anonymous_MetsDocumentID.ts';

export type xml_ns3_anonymous_MetsHdr = {
    ID?: string;
    ADMID?: Array<string>;
    CREATEDATE?: string;
    LASTMODDATE?: string;
    RECORDSTATUS?: string;
    agent?: Array<xml_ns3_anonymous_Agent>;
    altRecordID?: Array<xml_ns3_anonymous_AltRecordID>;
    metsDocumentID?: xml_ns3_anonymous_MetsDocumentID;
};

