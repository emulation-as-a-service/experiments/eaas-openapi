/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_fileURL } from './xml_ns0_fileURL.ts';
import type { xml_ns2_fileSystemType } from './xml_ns2_fileSystemType.ts';
import type { xml_ns2_mediumType } from './xml_ns2_mediumType.ts';
import type { xml_ns2_partitionTableType } from './xml_ns2_partitionTableType.ts';

export type xml_ns0_inputMedium = {
    content?: Array<xml_ns0_fileURL>;
    destination?: string;
    filesystem_type?: xml_ns2_fileSystemType;
    partition_table_type?: xml_ns2_partitionTableType;
    size_mb?: number;
    type?: xml_ns2_mediumType;
};

