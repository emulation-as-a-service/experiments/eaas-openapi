/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentRequest } from './ComponentRequest.ts';
import type { NodeTcpConfiguration } from './NodeTcpConfiguration.ts';

export type NodeTcpComponentRequest = (ComponentRequest & {
    config?: NodeTcpConfiguration;
});

