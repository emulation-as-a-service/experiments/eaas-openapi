/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Mptr = {
    href?: string;
    show?: string;
    title?: string;
    OTHERLOCTYPE?: string;
    type?: string;
    role?: string;
    actuate?: string;
    arcrole?: string;
    LOCTYPE?: string;
    ID?: string;
    CONTENTIDS?: Array<string>;
};

