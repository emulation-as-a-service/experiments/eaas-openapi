/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { MdSecType } from './MdSecType.ts';

export type AmdSecType = {
    techMD?: Array<MdSecType>;
    rightsMD?: Array<MdSecType>;
    sourceMD?: Array<MdSecType>;
    digiprovMD?: Array<MdSecType>;
    ID?: string;
};

