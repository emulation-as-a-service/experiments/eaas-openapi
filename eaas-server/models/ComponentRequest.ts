/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilRequestType } from './EmilRequestType.ts';

export type ComponentRequest = (EmilRequestType & {
    /**
     * The JSON object type.
     */
    type?: string;
});

