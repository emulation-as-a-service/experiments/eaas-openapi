/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AreaType = {
    BETYPE?: string;
    EXTENT?: string;
    END?: string;
    ID?: string;
    CONTENTIDS?: Array<string>;
    EXTTYPE?: string;
    FILEID?: any;
    SHAPE?: string;
    COORDS?: string;
    ADMID?: Array<any>;
    BEGIN?: string;
};

