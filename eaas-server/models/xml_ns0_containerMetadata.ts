/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_containerMetadata = {
    args?: Array<string>;
    dhcp?: boolean;
    process?: string;
    telnet?: boolean;
};

