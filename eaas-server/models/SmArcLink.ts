/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SmArcLink = {
    show?: string;
    from?: string;
    ARCTYPE?: string;
    arcrole?: string;
    ADMID?: Array<any>;
    title?: string;
    actuate?: string;
    to?: string;
    ID?: string;
    type?: string;
};

