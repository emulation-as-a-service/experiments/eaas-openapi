/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentResponse } from './xml_ns0_componentResponse.ts';

export type xml_ns0_componentStateResponse = (xml_ns0_componentResponse & {
    state?: string;
});

