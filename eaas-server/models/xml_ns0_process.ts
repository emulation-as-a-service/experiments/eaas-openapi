/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_process = {
    arg?: Array<string>;
    env?: Array<string>;
    workingDir?: string;
};

