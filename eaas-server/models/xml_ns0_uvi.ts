/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_machine } from './xml_ns0_machine.ts';
import type { xml_ns0_uviFile } from './xml_ns0_uviFile.ts';

export type xml_ns0_uvi = (xml_ns0_machine & {
    auxFiles?: Array<xml_ns0_uviFile>;
    uviFilename?: string;
    uviUrl?: string;
    uviWriteable?: boolean;
});

