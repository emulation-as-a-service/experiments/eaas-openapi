/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type ImageGeneralizationPatchResponse = (JaxbType & {
    status?: string;
    imageId?: string;
});

