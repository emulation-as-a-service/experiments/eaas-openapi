/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ImageGeneralizationPatchDescription = {
    name?: string;
    description?: string;
};

