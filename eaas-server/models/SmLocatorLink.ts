/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SmLocatorLink = {
    ID?: string;
    title?: string;
    role?: string;
    href?: string;
    label?: string;
    type?: string;
};

