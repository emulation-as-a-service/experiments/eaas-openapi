/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentRequest } from './ComponentRequest.ts';

export type SlirpComponentRequest = (ComponentRequest & {
    hwAddress?: string;
    network?: string;
    gateway?: string;
    netmask?: string;
    dhcp?: boolean;
    dnsServer?: string;
});

