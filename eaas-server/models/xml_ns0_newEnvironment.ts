/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_saveRevision } from './xml_ns0_saveRevision.ts';

export type xml_ns0_newEnvironment = (xml_ns0_saveRevision & {
    title?: string;
});

