/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilObjectEnvironment } from './xml_ns0_emilObjectEnvironment.ts';

export type xml_ns0_emilSessionEnvironment = (xml_ns0_emilObjectEnvironment & {
    baseEnv?: string;
    creationDate?: string;
    lastAccess?: string;
    userId?: string;
});

