/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_anonymous_ResourceType } from './xml_ns0_anonymous_ResourceType.ts';
import type { xml_ns1_binding } from './xml_ns1_binding.ts';
import type { xml_ns2_fileSystemType } from './xml_ns2_fileSystemType.ts';

export type xml_ns1_blobStoreBinding = (xml_ns1_binding & {
    fileSystemType?: xml_ns2_fileSystemType;
    offset?: number;
    resourceType?: xml_ns0_anonymous_ResourceType;
    mountFS?: boolean;
});

