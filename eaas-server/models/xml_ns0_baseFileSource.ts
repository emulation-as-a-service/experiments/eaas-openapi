/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns2_action } from './xml_ns2_action.ts';
import type { xml_ns2_archiveFormat } from './xml_ns2_archiveFormat.ts';

export type xml_ns0_baseFileSource = {
    action?: xml_ns2_action;
    compression_format?: xml_ns2_archiveFormat;
    name?: string;
};

