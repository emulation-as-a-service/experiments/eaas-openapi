/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Agent = {
    name?: string;
    note?: Array<string>;
    ID?: string;
    ROLE?: string;
    TYPE?: string;
    OTHERTYPE?: string;
    OTHERROLE?: string;
};

