/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_fileType } from './xml_ns3_fileType.ts';

export type xml_ns3_fileGrpType = {
    ID?: string;
    VERSDATE?: string;
    ADMID?: Array<string>;
    USE?: string;
    fileGrp?: Array<xml_ns3_fileGrpType>;
    file?: Array<xml_ns3_fileType>;
};

