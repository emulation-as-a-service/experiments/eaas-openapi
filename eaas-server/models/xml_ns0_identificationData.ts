/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_fileFormat } from './xml_ns0_fileFormat.ts';

export type xml_ns0_identificationData = {
    fileFormats?: Array<xml_ns0_fileFormat>;
};

