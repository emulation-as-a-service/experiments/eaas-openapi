/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilRequestType } from './EmilRequestType.ts';

export type SnapshotRequest = (EmilRequestType & {
    /**
     * The JSON object type.
     */
    type?: string;
    envId?: string;
    archive?: string;
    message?: string;
    author?: string;
    isRelativeMouse?: boolean;
    cleanRemovableDrives?: boolean;
    relativeMouse?: boolean;
});

