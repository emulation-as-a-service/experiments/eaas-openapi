/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_amdSecType } from './xml_ns3_amdSecType.ts';
import type { xml_ns3_anonymous_FileSec } from './xml_ns3_anonymous_FileSec.ts';
import type { xml_ns3_anonymous_MetsHdr } from './xml_ns3_anonymous_MetsHdr.ts';
import type { xml_ns3_anonymous_StructLink } from './xml_ns3_anonymous_StructLink.ts';
import type { xml_ns3_behaviorSecType } from './xml_ns3_behaviorSecType.ts';
import type { xml_ns3_mdSecType } from './xml_ns3_mdSecType.ts';
import type { xml_ns3_structMapType } from './xml_ns3_structMapType.ts';

export type xml_ns3_metsType = {
    ID?: string;
    OBJID?: string;
    LABEL?: string;
    TYPE?: string;
    PROFILE?: string;
    metsHdr?: xml_ns3_anonymous_MetsHdr;
    dmdSec?: Array<xml_ns3_mdSecType>;
    amdSec?: Array<xml_ns3_amdSecType>;
    fileSec?: xml_ns3_anonymous_FileSec;
    structMap?: Array<xml_ns3_structMapType>;
    structLink?: xml_ns3_anonymous_StructLink;
    behaviorSec?: Array<xml_ns3_behaviorSecType>;
};

