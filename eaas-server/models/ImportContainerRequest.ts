/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerType } from './ContainerType.ts';
import type { EmilRequestType } from './EmilRequestType.ts';

/**
 * ImportContainerRequest
 */
export type ImportContainerRequest = (EmilRequestType & {
    name?: string;
    imageType?: ContainerType;
    imageUrl?: string;
    runtimeId?: string;
    outputFolder?: string;
    inputFolder?: string;
    title?: string;
    processArgs?: Array<string>;
    processEnvs?: Array<string>;
    containerDigest?: string;
    guiRequired?: boolean;
    description?: string;
    author?: string;
    workingDir?: string;
    enableNetwork?: boolean;
    serviceContainer?: boolean;
    serviceContainerId?: string;
    archive?: string;
    customSubdir?: string;
});

