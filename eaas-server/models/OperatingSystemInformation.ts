/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OperatingSystemInformation = {
    puids?: Array<string>;
    label?: string;
    extensions?: Array<string>;
    id?: string;
};

