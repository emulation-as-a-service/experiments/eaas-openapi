/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns2_action {
    COPY = 'copy',
    EXTRACT = 'extract',
    RSYNC = 'rsync',
}
