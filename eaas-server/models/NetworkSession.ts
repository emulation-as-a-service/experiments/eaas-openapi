/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Session } from './Session.ts';

export type NetworkSession = Session;

