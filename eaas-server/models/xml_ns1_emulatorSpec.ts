/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_anonymous_Architecture } from './xml_ns0_anonymous_Architecture.ts';
import type { xml_ns0_anonymous_Machine } from './xml_ns0_anonymous_Machine.ts';

export type xml_ns1_emulatorSpec = {
    bean?: string;
    version?: string;
    containerVersion?: string;
    upstreamDigest?: string;
    ociSourceUrl?: string;
    containerName?: string;
    machine?: xml_ns0_anonymous_Machine;
    architecture?: xml_ns0_anonymous_Architecture;
};

