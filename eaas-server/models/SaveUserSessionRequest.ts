/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SnapshotRequest } from './SnapshotRequest.ts';

export type SaveUserSessionRequest = (SnapshotRequest & {
    envId?: string;
    objectId?: string;
    archiveId?: string;
});

