/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ContainerImageMetadata = {
    containerSourceUrl?: string;
    entryProcesses?: Array<string>;
    envVariables?: Array<string>;
    workingDir?: string;
    containerDigest?: string;
    tag?: string;
    emulatorType?: string;
    emulatorVersion?: string;
};

