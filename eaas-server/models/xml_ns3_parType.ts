/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_areaType } from './xml_ns3_areaType.ts';
import type { xml_ns3_seqType } from './xml_ns3_seqType.ts';

export type xml_ns3_parType = {
    ID?: string;
    area?: xml_ns3_areaType;
    seq?: xml_ns3_seqType;
};

