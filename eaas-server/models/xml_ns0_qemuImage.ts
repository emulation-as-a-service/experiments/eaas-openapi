/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_qemuImage = {
    'backing-filename'?: string;
    filename?: string;
    format?: string;
    'full-backing-filename'?: string;
    'virtual-size'?: string;
};

