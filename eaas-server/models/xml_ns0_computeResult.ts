/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_computeResult = {
    componentId?: string;
    environmentId?: string;
    resultBlob?: string;
    state?: string;
};

