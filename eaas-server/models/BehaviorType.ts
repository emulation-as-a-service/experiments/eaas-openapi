/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ObjectType } from './ObjectType.ts';

export type BehaviorType = {
    interfaceDef?: ObjectType;
    mechanism?: ObjectType;
    ID?: string;
    ADMID?: Array<any>;
    BTYPE?: string;
    GROUPID?: string;
    CREATED?: number;
    LABEL?: string;
    STRUCTID?: Array<any>;
};

