/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { MdRef } from './MdRef.ts';
import type { MdWrap } from './MdWrap.ts';

export type MdSecType = {
    mdWrap?: MdWrap;
    CREATED?: number;
    ADMID?: Array<any>;
    mdRef?: MdRef;
    ID?: string;
    STATUS?: string;
    GROUPID?: string;
};

