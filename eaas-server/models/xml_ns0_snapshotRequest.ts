/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';

export type xml_ns0_snapshotRequest = (xml_ns0_emilRequestType & {
    archive?: string;
    author?: string;
    cleanRemovableDrives?: boolean;
    envId?: string;
    isRelativeMouse?: boolean;
    message?: string;
    relativeMouse?: boolean;
});

