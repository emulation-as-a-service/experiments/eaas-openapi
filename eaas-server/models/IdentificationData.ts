/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FileFormat } from './FileFormat.ts';
import type { JaxbType } from './JaxbType.ts';

export type IdentificationData = (JaxbType & {
    fileFormats?: Array<FileFormat>;
});

