/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type NetworkEnvironmentElement = {
    envId?: string;
    macAddress?: string;
    serverPorts?: Array<number>;
    serverIp?: string;
    wildcard: boolean;
    label?: string;
    title?: string;
    fqdn?: string;
};

