/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_emilResponseType = {
    message?: string;
    status?: string;
};

