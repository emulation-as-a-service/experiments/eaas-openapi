/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilResponseType } from './EmilResponseType.ts';

export type TaskStateResponse = (EmilResponseType & {
    taskId?: string;
    isDone?: boolean;
    userData?: Record<string, string>;
    object?: string;
});

