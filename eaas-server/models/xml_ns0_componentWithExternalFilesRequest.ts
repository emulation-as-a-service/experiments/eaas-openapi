/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentRequest } from './xml_ns0_componentRequest.ts';
import type { xml_ns0_inputMedium } from './xml_ns0_inputMedium.ts';

export type xml_ns0_componentWithExternalFilesRequest = (xml_ns0_componentRequest & {
    input_data?: Array<xml_ns0_inputMedium>;
});

