/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type QemuImage = (JaxbType & {
    filename?: string;
    format?: string;
    'full-backing-filename'?: string;
    'backing-filename'?: string;
    'virtual-size'?: string;
});

