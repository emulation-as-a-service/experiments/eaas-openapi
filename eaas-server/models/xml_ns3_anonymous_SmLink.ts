/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_SmLink = {
    ID?: string;
    actuate?: string;
    arcrole?: string;
    from?: string;
    show?: string;
    title?: string;
    to?: string;
};

