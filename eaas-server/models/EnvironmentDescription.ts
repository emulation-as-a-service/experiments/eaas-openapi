/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EnvironmentDescription = {
    title?: string;
    os?: string;
};

