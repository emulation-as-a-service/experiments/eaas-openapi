/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DetachRequest } from './DetachRequest.ts';
import type { SessionResource } from './SessionResource.ts';

export type SessionRequest = (DetachRequest & {
    resources?: Array<SessionResource>;
});

