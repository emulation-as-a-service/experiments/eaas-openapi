/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_metsType } from './xml_ns3_metsType.ts';

export type xml_ns3_anonymous_Mets = xml_ns3_metsType;

