/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DHCPConfiguration } from './DHCPConfiguration.ts';
import type { EnvironmentNetworkConfiguration } from './EnvironmentNetworkConfiguration.ts';
import type { JaxbType } from './JaxbType.ts';

export type NetworkConfiguration = (JaxbType & {
    upstream_dns?: string;
    environments?: Array<EnvironmentNetworkConfiguration>;
    gateway?: string;
    archived_internet_date?: string;
    network?: string;
    dhcp?: DHCPConfiguration;
});

