/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilEnvironment } from './EmilEnvironment.ts';

export type EmilObjectEnvironment = (EmilEnvironment & {
    driveId?: number;
    objectId?: string;
    objectArchiveId?: string;
});

