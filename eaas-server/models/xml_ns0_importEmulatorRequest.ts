/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_containerImageMetadata } from './xml_ns0_containerImageMetadata.ts';
import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';

export type xml_ns0_importEmulatorRequest = (xml_ns0_emilRequestType & {
    imageUrl?: string;
    metadata?: xml_ns0_containerImageMetadata;
});

