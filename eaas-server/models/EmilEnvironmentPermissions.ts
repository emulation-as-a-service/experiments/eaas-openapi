/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EmilEnvironmentPermissions = {
    group?: string;
    user?: string;
};

