/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_input } from './xml_ns0_input.ts';
import type { xml_ns1_binding } from './xml_ns1_binding.ts';
import type { xml_ns1_environment } from './xml_ns1_environment.ts';
import type { xml_ns1_objectArchiveBinding } from './xml_ns1_objectArchiveBinding.ts';

export type xml_ns1_containerConfiguration = (xml_ns1_environment & {
    inputs?: Array<xml_ns0_input>;
    output?: string;
    input?: string;
    binding?: xml_ns1_binding;
    objectArchiveBinding?: xml_ns1_objectArchiveBinding;
    digest?: string;
});

