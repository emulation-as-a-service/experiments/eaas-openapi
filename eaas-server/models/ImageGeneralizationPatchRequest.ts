/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImageType } from './ImageType.ts';
import type { JaxbType } from './JaxbType.ts';

export type ImageGeneralizationPatchRequest = (JaxbType & {
    archive?: string;
    imageId?: string;
    imageType?: ImageType;
});

