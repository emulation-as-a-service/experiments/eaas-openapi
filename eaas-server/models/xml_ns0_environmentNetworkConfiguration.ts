/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_environmentNetworkConfiguration = {
    hostnames?: Array<string>;
    ip?: string;
    mac?: string;
    wildcard?: boolean;
};

