/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentSpec } from './xml_ns0_componentSpec.ts';

export type xml_ns0_computeRequest = {
    components?: Array<xml_ns0_componentSpec>;
    timeout?: number;
};

