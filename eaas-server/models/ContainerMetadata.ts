/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type ContainerMetadata = (JaxbType & {
    dhcp?: boolean;
    telnet?: boolean;
    process?: string;
    args?: Array<string>;
});

