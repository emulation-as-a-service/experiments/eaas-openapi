/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type MdRef = {
    href?: string;
    ID?: string;
    OTHERMDTYPE?: string;
    title?: string;
    CHECKSUMTYPE?: string;
    role?: string;
    MIMETYPE?: string;
    arcrole?: string;
    CHECKSUM?: string;
    OTHERLOCTYPE?: string;
    MDTYPEVERSION?: string;
    LOCTYPE?: string;
    XPTR?: string;
    actuate?: string;
    LABEL?: string;
    SIZE?: number;
    type?: string;
    MDTYPE?: string;
    show?: string;
    CREATED?: number;
};

