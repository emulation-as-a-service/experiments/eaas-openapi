/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_anonymous_DriveType } from './xml_ns0_anonymous_DriveType.ts';
import type { xml_ns1_device } from './xml_ns1_device.ts';

export type xml_ns1_drive = (xml_ns1_device & {
    data?: string;
    iface?: string;
    bus?: string;
    unit?: string;
    type?: xml_ns0_anonymous_DriveType;
    filesystem?: string;
    boot?: boolean;
    plugged?: boolean;
});

