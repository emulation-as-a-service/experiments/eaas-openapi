/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type FLocat = {
    href?: string;
    type?: string;
    USE?: string;
    show?: string;
    OTHERLOCTYPE?: string;
    actuate?: string;
    LOCTYPE?: string;
    ID?: string;
    title?: string;
    role?: string;
    arcrole?: string;
};

