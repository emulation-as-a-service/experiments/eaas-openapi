/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { XmlData } from './XmlData.ts';

export type FContent = {
    binData?: string;
    xmlData?: XmlData;
    ID?: string;
    USE?: string;
};

