/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns1_digitalObjectMetadata = {
    customData?: any;
    description?: string;
    id?: string;
    metsData?: string;
    summary?: string;
    thumbnail?: string;
    title?: string;
    wikiDataId?: string;
};

