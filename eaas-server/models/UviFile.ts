/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UviFile = {
    url?: string;
    filename?: string;
};

