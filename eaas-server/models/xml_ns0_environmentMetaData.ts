/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';

export type xml_ns0_environmentMetaData = (xml_ns0_emilResponseType & {
    mediaChangeSupport?: boolean;
});

