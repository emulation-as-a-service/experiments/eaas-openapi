/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_html5Options } from './xml_ns1_html5Options.ts';
import type { xml_ns1_kbdOptions } from './xml_ns1_kbdOptions.ts';
import type { xml_ns1_timeOptions } from './xml_ns1_timeOptions.ts';

export type xml_ns1_uiOptions = {
    audio_system?: string;
    forwarding_system?: string;
    html5?: xml_ns1_html5Options;
    input?: xml_ns1_kbdOptions;
    time?: xml_ns1_timeOptions;
};

