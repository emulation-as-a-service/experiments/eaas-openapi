/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentSpec_ComputeRequest } from './ComponentSpec_ComputeRequest.ts';
import type { JaxbType } from './JaxbType.ts';

export type ComputeRequest = (JaxbType & {
    components?: Array<ComponentSpec_ComputeRequest>;
    timeout?: number;
});

