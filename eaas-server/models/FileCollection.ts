/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FileCollectionEntry } from './FileCollectionEntry.ts';
import type { JaxbType } from './JaxbType.ts';

export type FileCollection = (JaxbType & {
    label?: string;
    id?: string;
    archive?: string;
    file?: Array<FileCollectionEntry>;
});

