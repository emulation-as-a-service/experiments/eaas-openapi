/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { TaskStateResponse } from './TaskStateResponse.ts';

export type RemoteExportResponse = TaskStateResponse;

