/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_detachRequest } from './xml_ns0_detachRequest.ts';
import type { xml_ns0_sessionResource } from './xml_ns0_sessionResource.ts';

export type xml_ns0_sessionRequest = (xml_ns0_detachRequest & {
    resources?: Array<xml_ns0_sessionResource>;
});

