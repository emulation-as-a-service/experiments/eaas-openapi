/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentConfiguration } from './xml_ns0_componentConfiguration.ts';

export type xml_ns0_networkSwitchConfiguration = xml_ns0_componentConfiguration;

