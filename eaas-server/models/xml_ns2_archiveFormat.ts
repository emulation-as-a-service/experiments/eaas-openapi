/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns2_archiveFormat {
    ZIP = 'zip',
    TAR = 'tar',
    SIMG = 'simg',
    DOCKER = 'docker',
}
