/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';

export type xml_ns0_userInfoResponse = (xml_ns0_emilResponseType & {
    fullName?: string;
    role?: string;
    userId?: string;
    username?: string;
});

