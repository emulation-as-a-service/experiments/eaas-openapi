/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Agent } from './Agent.ts';
import type { AltRecordID } from './AltRecordID.ts';
import type { MetsDocumentID } from './MetsDocumentID.ts';

export type MetsHdr = {
    agent?: Array<Agent>;
    altRecordID?: Array<AltRecordID>;
    metsDocumentID?: MetsDocumentID;
    ID?: string;
    ADMID?: Array<any>;
    CREATEDATE?: number;
    LASTMODDATE?: number;
    RECORDSTATUS?: string;
};

