/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AreaType } from './AreaType.ts';
import type { ParType } from './ParType.ts';
import type { SeqType } from './SeqType.ts';

export type Fptr = {
    par?: ParType;
    seq?: SeqType;
    area?: AreaType;
    ID?: string;
    CONTENTIDS?: Array<string>;
    FILEID?: string;
};

