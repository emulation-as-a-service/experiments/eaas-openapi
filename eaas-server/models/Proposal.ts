/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ClassificationResult } from './ClassificationResult.ts';
import type { MediumType } from './MediumType.ts';

export type Proposal = {
    image_url?: string;
    image_type?: MediumType;
    result?: ClassificationResult;
};

