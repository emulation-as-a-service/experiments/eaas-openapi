/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_importImageRequest = {
    imageType?: string;
    label?: string;
    url?: string;
};

