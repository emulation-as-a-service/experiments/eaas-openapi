/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { OperatingSystemInformation } from './OperatingSystemInformation.ts';

export type OperatingSystems = (JaxbType & {
    operatingSystemInformations?: Array<OperatingSystemInformation>;
});

