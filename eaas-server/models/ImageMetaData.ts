/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AbstractMetaData } from './AbstractMetaData.ts';

export type ImageMetaData = AbstractMetaData;

