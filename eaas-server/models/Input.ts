/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Input = {
    binding?: string;
    destination?: string;
};

