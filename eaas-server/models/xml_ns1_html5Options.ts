/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns1_html5Options = {
    pointer_lock?: boolean;
    crt?: string;
};

