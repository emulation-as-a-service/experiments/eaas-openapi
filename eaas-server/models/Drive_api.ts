/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Device } from './Device.ts';
import type { DriveType } from './DriveType.ts';

export type Drive_api = (Device & {
    data?: string;
    iface?: string;
    bus?: string;
    unit?: string;
    type?: DriveType;
    filesystem?: string;
    boot?: boolean;
    plugged?: boolean;
});

