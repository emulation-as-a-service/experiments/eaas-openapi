/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_containerNetworkingType } from './xml_ns0_containerNetworkingType.ts';
import type { xml_ns0_emilEnvironment } from './xml_ns0_emilEnvironment.ts';

export type xml_ns0_emilContainerEnvironment = (xml_ns0_emilEnvironment & {
    args?: Array<string>;
    digest?: string;
    env?: Array<string>;
    input?: string;
    networking?: xml_ns0_containerNetworkingType;
    output?: string;
    runtimeId?: string;
    serviceContainer?: boolean;
});

