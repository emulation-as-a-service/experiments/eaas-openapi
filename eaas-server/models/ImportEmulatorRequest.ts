/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerImageMetadata } from './ContainerImageMetadata.ts';
import type { EmilRequestType } from './EmilRequestType.ts';

export type ImportEmulatorRequest = (EmilRequestType & {
    imageUrl?: string;
    metadata?: ContainerImageMetadata;
});

