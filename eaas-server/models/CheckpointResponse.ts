/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type CheckpointResponse = (JaxbType & {
    environment_id?: string;
});

