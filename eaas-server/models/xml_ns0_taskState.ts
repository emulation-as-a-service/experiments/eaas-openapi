/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_taskState = {
    done?: boolean;
    failed?: boolean;
    result?: string;
    taskId?: string;
};

