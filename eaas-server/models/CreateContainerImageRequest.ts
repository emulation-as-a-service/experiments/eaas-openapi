/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerType } from './ContainerType.ts';

export type CreateContainerImageRequest = {
    tag?: string;
    digest?: string;
    containerType?: ContainerType;
    urlString?: string;
    checkForExistingDigest: boolean;
};

