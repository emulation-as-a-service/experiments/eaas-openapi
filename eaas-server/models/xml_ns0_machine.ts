/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentWithExternalFilesRequest } from './xml_ns0_componentWithExternalFilesRequest.ts';
import type { xml_ns0_drive } from './xml_ns0_drive.ts';
import type { xml_ns0_linuxRuntimeContainerReq } from './xml_ns0_linuxRuntimeContainerReq.ts';
import type { xml_ns0_userMedium } from './xml_ns0_userMedium.ts';

export type xml_ns0_machine = (xml_ns0_componentWithExternalFilesRequest & {
    archive?: string;
    drives?: Array<xml_ns0_drive>;
    emulatorVersion?: string;
    environment?: string;
    hasOutput?: boolean;
    headless?: boolean;
    keyboardLayout?: string;
    keyboardModel?: string;
    linuxRuntimeData?: xml_ns0_linuxRuntimeContainerReq;
    lockEnvironment?: boolean;
    nic?: string;
    object?: string;
    objectArchive?: string;
    outputDriveId?: string;
    sessionLifetime?: number;
    software?: string;
    userMedia?: Array<xml_ns0_userMedium>;
});

