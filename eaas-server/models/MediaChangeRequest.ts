/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type MediaChangeRequest = (JaxbType & {
    archiveId?: string;
    objectId?: string;
    driveId?: string;
    label?: string;
});

