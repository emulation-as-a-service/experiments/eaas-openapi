/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type EmilSoftwareObject = {
    isPublic: boolean;
    isOperatingSystem: boolean;
    id?: string;
    objectId?: string;
    licenseInformation?: string;
    allowedInstances: number;
    nativeFMTs?: Array<string>;
    importFMTs?: Array<string>;
    exportFMTs?: Array<string>;
    archiveId?: string;
    label?: string;
    QID?: string;
};

