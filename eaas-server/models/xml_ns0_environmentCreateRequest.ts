/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_driveSetting } from './xml_ns0_driveSetting.ts';

export type xml_ns0_environmentCreateRequest = {
    driveSettings?: Array<xml_ns0_driveSetting>;
    enablePrinting?: boolean;
    enableRelativeMouse?: boolean;
    label?: string;
    nativeConfig?: string;
    operatingSystemId?: string;
    romId?: string;
    romLabel?: string;
    shutdownByOs?: boolean;
    templateId?: string;
    useWebRTC?: boolean;
    useXpra?: boolean;
    xpraEncoding?: string;
};

