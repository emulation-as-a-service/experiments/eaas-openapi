/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_emilEnvironmentOwner = {
    usergroup?: string;
    username?: string;
};

