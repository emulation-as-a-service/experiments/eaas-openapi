/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentResponse } from './xml_ns0_componentResponse.ts';
import type { xml_ns0_removableMedia } from './xml_ns0_removableMedia.ts';

export type xml_ns0_machineComponentResponse = (xml_ns0_componentResponse & {
    driveId?: number;
    removableMediaList?: Array<xml_ns0_removableMedia>;
});

