/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_emulationEnvironment } from './xml_ns1_emulationEnvironment.ts';

export type xml_ns0_machineConfigurationTemplate = xml_ns1_emulationEnvironment;

