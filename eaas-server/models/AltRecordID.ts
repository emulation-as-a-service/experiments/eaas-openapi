/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AltRecordID = {
    value?: string;
    ID?: string;
    TYPE?: string;
};

