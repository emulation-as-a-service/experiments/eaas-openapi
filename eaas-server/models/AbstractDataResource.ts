/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type AbstractDataResource = (JaxbType & {
    id?: string;
    dataResourceType?: string;
});

