/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DriveDataSource } from './DriveDataSource.ts';
import type { JaxbType } from './JaxbType.ts';

export type Drive = (JaxbType & {
    id?: string;
    data?: DriveDataSource;
    bootable?: boolean;
});

