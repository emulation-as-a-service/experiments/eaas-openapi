/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DivType } from './DivType.ts';

export type StructMapType = {
    div?: DivType;
    ID?: string;
    TYPE?: string;
    LABEL?: string;
};

