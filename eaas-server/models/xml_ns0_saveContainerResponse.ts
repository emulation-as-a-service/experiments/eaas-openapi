/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';

/**
 * SaveContainerResponse: respond with status and possible id of saved image
 */
export type xml_ns0_saveContainerResponse = (xml_ns0_emilResponseType & {
    id?: string;
});

