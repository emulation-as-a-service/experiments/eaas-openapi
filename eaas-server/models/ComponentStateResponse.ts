/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentResponse } from './ComponentResponse.ts';

export type ComponentStateResponse = (ComponentResponse & {
    state?: string;
});

