/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_content = {
    type?: string;
    wikidata?: string;
};

