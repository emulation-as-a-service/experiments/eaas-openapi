/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SmLink = {
    arcrole?: string;
    actuate?: string;
    show?: string;
    ID?: string;
    to?: string;
    title?: string;
    from?: string;
};

