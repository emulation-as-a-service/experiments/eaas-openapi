/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Architecture = {
    id?: string;
    name?: string;
};

