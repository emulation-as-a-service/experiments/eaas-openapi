/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentConfiguration } from './ComponentConfiguration.ts';
import type { EnvironmentDescription } from './EnvironmentDescription.ts';

export type Environment = (ComponentConfiguration & {
    id?: string;
    timestamp?: string;
    description?: EnvironmentDescription;
    metaDataVersion?: string;
    userTag?: string;
    configurationType?: string;
    deleted?: boolean;
});

