/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentSpec } from './ComponentSpec.ts';
import type { JaxbType } from './JaxbType.ts';
import type { TcpGatewayConfig } from './TcpGatewayConfig.ts';

export type NetworkRequest = (JaxbType & {
    components?: Array<ComponentSpec>;
    networkEnvironmentId?: string;
    hasInternet?: boolean;
    enableDhcp?: boolean;
    dhcpNetworkAddress?: string;
    dhcpNetworkMask?: string;
    hasTcpGateway?: boolean;
    gateway?: string;
    network?: string;
    tcpGatewayConfig?: TcpGatewayConfig;
});

