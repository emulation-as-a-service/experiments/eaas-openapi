/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_driveDataSource } from './xml_ns0_driveDataSource.ts';

export type xml_ns0_softwareDataSource = (xml_ns0_driveDataSource & {
    id?: string;
});

