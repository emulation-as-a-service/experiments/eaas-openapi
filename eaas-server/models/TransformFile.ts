/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TransformFile = {
    TRANSFORMBEHAVIOR?: any;
    TRANSFORMTYPE?: string;
    ID?: string;
    TRANSFORMORDER?: number;
    TRANSFORMALGORITHM?: string;
    TRANSFORMKEY?: string;
};

