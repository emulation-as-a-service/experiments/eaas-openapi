/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilObjectEnvironment } from './EmilObjectEnvironment.ts';

export type EmilSessionEnvironment = (EmilObjectEnvironment & {
    baseEnv?: string;
    userId?: string;
    creationDate?: string;
    lastAccess?: string;
});

