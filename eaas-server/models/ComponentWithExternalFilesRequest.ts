/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentRequest } from './ComponentRequest.ts';
import type { InputMedium } from './InputMedium.ts';

export type ComponentWithExternalFilesRequest = (ComponentRequest & {
    input_data?: Array<InputMedium>;
});

