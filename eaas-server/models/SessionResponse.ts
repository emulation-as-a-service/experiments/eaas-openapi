/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { NetworkRequest } from './NetworkRequest.ts';
import type { SessionComponent } from './SessionComponent.ts';

export type SessionResponse = (JaxbType & {
    components?: Array<SessionComponent>;
    network?: NetworkRequest;
});

