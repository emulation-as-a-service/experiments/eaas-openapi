/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum PartitionTableType {
    MBR = 'mbr',
    GPT = 'gpt',
    NONE = 'none',
}
