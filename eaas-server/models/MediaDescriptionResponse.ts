/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ClassificationResult } from './ClassificationResult.ts';
import type { DigitalObjectMetadata } from './DigitalObjectMetadata.ts';
import type { EmilResponseType } from './EmilResponseType.ts';
import type { FileCollection } from './FileCollection.ts';

export type MediaDescriptionResponse = (EmilResponseType & {
    mediaItems?: FileCollection;
    metadata?: DigitalObjectMetadata;
    objectEnvironments?: ClassificationResult;
});

