/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns2_mediumType {
    HDD = 'hdd',
    CDROM = 'cdrom',
    FLOPPY = 'floppy',
}
