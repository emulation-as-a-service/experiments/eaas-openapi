/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentRequest } from './xml_ns0_componentRequest.ts';
import type { xml_ns0_networkSwitchConfiguration } from './xml_ns0_networkSwitchConfiguration.ts';

export type xml_ns0_switchComponentRequest = (xml_ns0_componentRequest & {
    config?: xml_ns0_networkSwitchConfiguration;
});

