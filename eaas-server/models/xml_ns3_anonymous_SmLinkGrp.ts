/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_SmArcLink } from './xml_ns3_anonymous_SmArcLink.ts';
import type { xml_ns3_anonymous_SmLocatorLink } from './xml_ns3_anonymous_SmLocatorLink.ts';

export type xml_ns3_anonymous_SmLinkGrp = {
    ID?: string;
    ARCLINKORDER?: string;
    type?: string;
    role?: string;
    title?: string;
    smLocatorLink?: Array<xml_ns3_anonymous_SmLocatorLink>;
    smArcLink?: Array<xml_ns3_anonymous_SmArcLink>;
};

