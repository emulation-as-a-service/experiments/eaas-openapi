/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerConfiguration } from './ContainerConfiguration.ts';

export type DockerContainerConfiguration = (ContainerConfiguration & {
    image?: string;
});

