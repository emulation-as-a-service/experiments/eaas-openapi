/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_FileGrp } from './xml_ns3_anonymous_FileGrp.ts';

export type xml_ns3_anonymous_FileSec = {
    ID?: string;
    fileGrp?: Array<xml_ns3_anonymous_FileGrp>;
};

