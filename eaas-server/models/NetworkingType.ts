/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type NetworkingType = {
    enableInternet: boolean;
    serverMode: boolean;
    localServerMode: boolean;
    enableSocks: boolean;
    serverPort?: string;
    serverIp?: string;
    gwPrivateIp?: string;
    gwPrivateMask?: string;
    connectEnvs: boolean;
    helpText?: string;
};

