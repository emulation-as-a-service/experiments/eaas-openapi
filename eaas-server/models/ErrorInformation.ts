/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type ErrorInformation = (JaxbType & {
    stacktrace?: string;
    detail?: string;
    error?: string;
});

