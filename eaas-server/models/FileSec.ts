/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { FileGrp } from './FileGrp.ts';

export type FileSec = {
    fileGrp?: Array<FileGrp>;
    ID?: string;
};

