/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilResponseType } from './EmilResponseType.ts';

export type DigitalObjectMetadataResponse = (EmilResponseType & {
    description?: string;
    summary?: string;
    thumbnail?: string;
    title?: string;
    wikiDataId?: string;
    customData?: Record<string, string>;
});

