/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Drive_api } from './Drive_api.ts';
import type { JaxbType } from './JaxbType.ts';

export type DriveSetting = (JaxbType & {
    drive?: Drive_api;
    imageId?: string;
    imageArchive?: string;
    objectId?: string;
    objectArchive?: string;
    driveIndex?: number;
    operatingSystem?: string;
});

