/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TcpGatewayConfig = {
    socks: boolean;
    serverPort?: string;
    serverIp?: string;
};

