/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_anonymous_DriveType } from './xml_ns0_anonymous_DriveType.ts';
import type { xml_ns0_anonymous_ResourceType } from './xml_ns0_anonymous_ResourceType.ts';
import type { xml_ns1_binding } from './xml_ns1_binding.ts';

export type xml_ns1_file = (xml_ns1_binding & {
    archive?: string;
    isDefault?: boolean;
    label?: string;
    objectId?: string;
    order?: string;
    resourceType?: xml_ns0_anonymous_ResourceType;
    type?: xml_ns0_anonymous_DriveType;
});

