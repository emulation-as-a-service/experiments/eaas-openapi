/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_divType } from './xml_ns3_divType.ts';

export type xml_ns3_structMapType = {
    ID?: string;
    TYPE?: string;
    LABEL?: string;
    div?: xml_ns3_divType;
};

