/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_fileGrpType } from './xml_ns3_fileGrpType.ts';

export type xml_ns3_anonymous_FileGrp = xml_ns3_fileGrpType;

