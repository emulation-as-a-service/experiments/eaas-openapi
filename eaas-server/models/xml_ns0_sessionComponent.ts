/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_networkResponse } from './xml_ns0_networkResponse.ts';

export type xml_ns0_sessionComponent = {
    componentId?: string;
    environmentId?: string;
    networkData?: xml_ns0_networkResponse;
    networkLabel?: string;
    type?: string;
};

