/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns0_containerType {
    ROOTFS = 'rootfs',
    SIMG = 'simg',
    DOCKERHUB = 'dockerhub',
    READYMADE = 'readymade',
}
