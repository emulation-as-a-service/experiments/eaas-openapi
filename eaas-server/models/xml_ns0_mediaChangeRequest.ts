/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_mediaChangeRequest = {
    archiveId?: string;
    driveId?: string;
    label?: string;
    objectId?: string;
};

