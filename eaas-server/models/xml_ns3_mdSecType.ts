/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_anonymous_MdRef } from './xml_ns3_anonymous_MdRef.ts';
import type { xml_ns3_anonymous_MdWrap } from './xml_ns3_anonymous_MdWrap.ts';

export type xml_ns3_mdSecType = {
    ADMID?: Array<string>;
    CREATED?: string;
    GROUPID?: string;
    ID?: string;
    STATUS?: string;
    mdRef?: xml_ns3_anonymous_MdRef;
    mdWrap?: xml_ns3_anonymous_MdWrap;
};

