/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DriveDataSource } from './DriveDataSource.ts';

export type ImageDataSource = (DriveDataSource & {
    id?: string;
});

