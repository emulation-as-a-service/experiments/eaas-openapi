/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_driveDataSource } from './xml_ns0_driveDataSource.ts';
import type { xml_ns2_mediumType } from './xml_ns2_mediumType.ts';

export type xml_ns0_userMedium = (xml_ns0_driveDataSource & {
    mediumType?: xml_ns2_mediumType;
    name?: string;
    url?: string;
});

