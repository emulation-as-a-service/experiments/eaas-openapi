/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SaveDerivateRequest } from './SaveDerivateRequest.ts';

export type SaveCreatedEnvironmentRequest = (SaveDerivateRequest & {
    title?: string;
});

