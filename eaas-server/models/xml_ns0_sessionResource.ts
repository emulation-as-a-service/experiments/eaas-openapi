/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_type } from './xml_ns0_type.ts';

export type xml_ns0_sessionResource = {
    id?: string;
    keepalive_url?: string;
    type?: xml_ns0_type;
};

