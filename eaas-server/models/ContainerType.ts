/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum ContainerType {
    ROOTFS = 'rootfs',
    SIMG = 'simg',
    DOCKERHUB = 'dockerhub',
    READYMADE = 'readymade',
}
