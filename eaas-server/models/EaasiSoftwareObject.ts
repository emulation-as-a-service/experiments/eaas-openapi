/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';
import type { SoftwarePackage } from './SoftwarePackage.ts';

export type EaasiSoftwareObject = (JaxbType & {
    metsData?: string;
    softwarePackage?: SoftwarePackage;
});

