/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_MetsDocumentID = {
    ID?: string;
    TYPE?: string;
    '(value)'?: string;
};

