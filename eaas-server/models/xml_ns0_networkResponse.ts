/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_networkResponse = {
    id?: string;
    isLocalMode?: boolean;
    networkUrls?: any;
};

