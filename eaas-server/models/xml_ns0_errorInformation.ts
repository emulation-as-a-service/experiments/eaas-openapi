/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_errorInformation = {
    detail?: string;
    error?: string;
    stacktrace?: string;
};

