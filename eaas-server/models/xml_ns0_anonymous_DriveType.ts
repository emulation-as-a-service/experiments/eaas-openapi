/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns0_anonymous_DriveType {
    CDROM = 'cdrom',
    DISK = 'disk',
    FLOPPY = 'floppy',
    CART = 'cart',
}
