/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_networkEnvironmentElement } from './xml_ns0_networkEnvironmentElement.ts';
import type { xml_ns0_networkEnvironmentNetworkingType } from './xml_ns0_networkEnvironmentNetworkingType.ts';

export type xml_ns0_networkEnvironment = {
    description?: string;
    dnsServiceEnvId?: string;
    emilEnvironments?: Array<xml_ns0_networkEnvironmentElement>;
    envId?: string;
    gateway?: string;
    linuxArchiveProxyEnvId?: string;
    network?: string;
    networking?: xml_ns0_networkEnvironmentNetworkingType;
    smbServiceEnvId?: string;
    startupEnvId?: string;
    title?: string;
    type?: string;
    upstream_dns?: string;
};

