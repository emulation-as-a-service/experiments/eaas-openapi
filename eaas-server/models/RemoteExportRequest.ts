/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilRequestType } from './EmilRequestType.ts';

export type RemoteExportRequest = (EmilRequestType & {
    wsHost?: string;
    envId?: Array<string>;
    exportObjectEmbedded?: boolean;
    objectArchiveHost?: string;
});

