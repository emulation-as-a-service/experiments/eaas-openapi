/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ContainerNetworkingType } from './ContainerNetworkingType.ts';
import type { EmilRequestType } from './EmilRequestType.ts';

export type UpdateContainerRequest = (EmilRequestType & {
    networking?: ContainerNetworkingType;
    id?: string;
    title?: string;
    description?: string;
    outputFolder?: string;
    inputFolder?: string;
    processArgs?: Array<string>;
    processEnvs?: Array<string>;
    author?: string;
    containerRuntimeId?: string;
});

