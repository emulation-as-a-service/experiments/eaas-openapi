/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_environmentInfo = {
    id?: string;
    label?: string;
    objectEnvironment?: boolean;
};

