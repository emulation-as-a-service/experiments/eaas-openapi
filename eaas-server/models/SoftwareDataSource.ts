/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DriveDataSource } from './DriveDataSource.ts';

export type SoftwareDataSource = (DriveDataSource & {
    id?: string;
});

