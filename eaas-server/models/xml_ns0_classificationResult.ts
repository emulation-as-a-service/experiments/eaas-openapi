/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilResponseType } from './xml_ns0_emilResponseType.ts';
import type { xml_ns0_environmentInfo } from './xml_ns0_environmentInfo.ts';
import type { xml_ns0_operatingSystem } from './xml_ns0_operatingSystem.ts';

export type xml_ns0_classificationResult = (xml_ns0_emilResponseType & {
    environmentList?: Array<xml_ns0_environmentInfo>;
    fileFormatMap?: any;
    manualEnvironmentList?: Array<xml_ns0_environmentInfo>;
    mediaFormats?: any;
    objectId?: string;
    suggested?: Array<xml_ns0_operatingSystem>;
    userDescription?: string;
});

