/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ComponentSpec_ComputeRequest = {
    componentId?: string;
    environmentId?: string;
    shouldSaveEnvironment: boolean;
    saveEnvironmentLabel?: string;
};

