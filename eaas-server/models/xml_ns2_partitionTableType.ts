/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum xml_ns2_partitionTableType {
    MBR = 'mbr',
    GPT = 'gpt',
    NONE = 'none',
}
