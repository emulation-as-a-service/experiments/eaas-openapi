/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AmdSecType } from './AmdSecType.ts';
import type { BehaviorSecType } from './BehaviorSecType.ts';
import type { FileSec } from './FileSec.ts';
import type { JaxbType } from './JaxbType.ts';
import type { MdSecType } from './MdSecType.ts';
import type { MetsHdr } from './MetsHdr.ts';
import type { StructLink } from './StructLink.ts';
import type { StructMapType } from './StructMapType.ts';

export type MetsType = (JaxbType & {
    metsHdr?: MetsHdr;
    dmdSec?: Array<MdSecType>;
    amdSec?: Array<AmdSecType>;
    fileSec?: FileSec;
    structMap?: Array<StructMapType>;
    structLink?: StructLink;
    behaviorSec?: Array<BehaviorSecType>;
    ID?: string;
    TYPE?: string;
    OBJID?: string;
    PROFILE?: string;
    LABEL?: string;
});

