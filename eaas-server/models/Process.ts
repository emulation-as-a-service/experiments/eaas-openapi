/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Process = {
    workingDir?: string;
    arg?: Array<string>;
    env?: Array<string>;
};

