/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_containerNetworkingType } from './xml_ns0_containerNetworkingType.ts';

export type xml_ns0_networkEnvironmentNetworkingType = (xml_ns0_containerNetworkingType & {
    allowExternalConnections?: boolean;
    archiveInternetDate?: string;
    dhcpNetworkAddress?: string;
    dhcpNetworkMask?: string;
    isArchivedInternetEnabled?: boolean;
});

