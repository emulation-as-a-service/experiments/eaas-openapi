/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_containerType } from './xml_ns0_containerType.ts';
import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';

/**
 * ImportContainerRequest
 */
export type xml_ns0_importContainerRequest = (xml_ns0_emilRequestType & {
    archive?: string;
    author?: string;
    containerDigest?: string;
    customSubdir?: string;
    description?: string;
    enableNetwork?: boolean;
    guiRequired?: boolean;
    imageType?: xml_ns0_containerType;
    imageUrl?: string;
    inputFolder?: string;
    /**
     * the name
     */
    name?: string;
    outputFolder?: string;
    processArgs?: Array<string>;
    processEnvs?: Array<string>;
    runtimeId?: string;
    serviceContainer?: boolean;
    serviceContainerId?: string;
    title?: string;
    workingDir?: string;
});

