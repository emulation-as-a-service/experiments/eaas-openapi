/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilRequestType } from './EmilRequestType.ts';

export type UpdateLatestEmulatorRequest = (EmilRequestType & {
    version?: string;
    emulatorName?: string;
});

