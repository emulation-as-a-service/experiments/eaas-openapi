/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilEnvironment } from './xml_ns0_emilEnvironment.ts';

export type xml_ns0_emilObjectEnvironment = (xml_ns0_emilEnvironment & {
    driveId?: number;
    objectArchiveId?: string;
    objectId?: string;
});

