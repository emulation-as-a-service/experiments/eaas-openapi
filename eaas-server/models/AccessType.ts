/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum AccessType {
    COW = 'cow',
    COPY = 'copy',
}
