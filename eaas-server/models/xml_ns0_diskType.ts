/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_content } from './xml_ns0_content.ts';

export type xml_ns0_diskType = {
    content?: Array<xml_ns0_content>;
    localAlias?: string;
    path?: string;
    size?: string;
    type?: string;
};

