/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComputeResult } from './ComputeResult.ts';
import type { JaxbType } from './JaxbType.ts';

export type ComputeResponse = (JaxbType & {
    id?: string;
    result?: Array<ComputeResult>;
});

