/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AreaType } from './AreaType.ts';
import type { ParType } from './ParType.ts';

export type SeqType = {
    area?: Array<AreaType>;
    par?: Array<ParType>;
    ID?: string;
};

