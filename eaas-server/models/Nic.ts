/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Device } from './Device.ts';

export type Nic = (Device & {
    hwaddress?: string;
});

