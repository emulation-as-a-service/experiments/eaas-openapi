/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Fptr } from './Fptr.ts';
import type { Mptr } from './Mptr.ts';

export type DivType = {
    mptr?: Array<Mptr>;
    fptr?: Array<Fptr>;
    div?: Array<DivType>;
    ID?: string;
    TYPE?: string;
    ADMID?: Array<any>;
    DMDID?: Array<any>;
    label?: string;
    ORDER?: number;
    CONTENTIDS?: Array<string>;
    ORDERLABEL?: string;
    LABEL?: string;
};

