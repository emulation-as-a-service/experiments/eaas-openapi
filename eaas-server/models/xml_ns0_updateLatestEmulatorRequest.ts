/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_emilRequestType } from './xml_ns0_emilRequestType.ts';

export type xml_ns0_updateLatestEmulatorRequest = (xml_ns0_emilRequestType & {
    emulatorName?: string;
    version?: string;
});

