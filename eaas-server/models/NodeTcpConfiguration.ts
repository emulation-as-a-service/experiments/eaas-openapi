/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentConfiguration } from './ComponentConfiguration.ts';

export type NodeTcpConfiguration = (ComponentConfiguration & {
    socksMode?: boolean;
    destIp?: string;
    dhcpNetworkAddress?: string;
    dhcpNetworkMask?: string;
    dhcp?: boolean;
    socksUser?: string;
    hwAddress?: string;
    socksPasswd?: string;
    destPort?: string;
});

