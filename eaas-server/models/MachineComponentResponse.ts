/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ComponentResponse } from './ComponentResponse.ts';
import type { RemovableMedia } from './RemovableMedia.ts';

export type MachineComponentResponse = (ComponentResponse & {
    driveId?: number;
    removableMediaList?: Array<RemovableMedia>;
});

