/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type HandleRequest = (JaxbType & {
    handle?: string;
    value?: string;
    index?: number;
});

