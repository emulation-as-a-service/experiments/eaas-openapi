/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_driveDataSource } from './xml_ns0_driveDataSource.ts';

export type xml_ns0_objectDataSource = (xml_ns0_driveDataSource & {
    archive?: string;
    id?: string;
});

