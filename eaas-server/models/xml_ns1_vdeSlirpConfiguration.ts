/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_componentConfiguration } from './xml_ns0_componentConfiguration.ts';

export type xml_ns1_vdeSlirpConfiguration = (xml_ns0_componentConfiguration & {
    dhcp?: boolean;
    dnsServer?: string;
    gateway?: string;
    hwAddress?: string;
    netmask?: string;
    network?: string;
});

