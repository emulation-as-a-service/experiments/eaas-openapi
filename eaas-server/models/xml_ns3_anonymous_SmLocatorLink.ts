/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns3_anonymous_SmLocatorLink = {
    ID?: string;
    href?: string;
    label?: string;
    role?: string;
    title?: string;
    type?: string;
};

