/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns1_softwarePackage = {
    QID?: string;
    archive?: string;
    deleted?: boolean;
    description?: string;
    documentation?: string;
    infoSource?: string;
    isOperatingSystem?: boolean;
    isPublic?: boolean;
    language?: string;
    licence?: string;
    location?: string;
    name?: string;
    numSeats?: number;
    objectId?: string;
    releaseDate?: string;
    supportedFileFormat?: Array<string>;
    timestamp?: string;
};

