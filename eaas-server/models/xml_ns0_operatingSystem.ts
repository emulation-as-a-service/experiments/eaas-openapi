/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_environmentInfo } from './xml_ns0_environmentInfo.ts';

export type xml_ns0_operatingSystem = {
    defaultEnvironment?: xml_ns0_environmentInfo;
    id?: string;
    label?: string;
};

