/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns0_containerImageMetadata = {
    containerDigest?: string;
    containerSourceUrl?: string;
    emulatorType?: string;
    emulatorVersion?: string;
    entryProcesses?: Array<string>;
    envVariables?: Array<string>;
    tag?: string;
    workingDir?: string;
};

