/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EmilResponseType } from './EmilResponseType.ts';

export type ObjectArchivesResponse = (EmilResponseType & {
    archives?: Array<string>;
});

