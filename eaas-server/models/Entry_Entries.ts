/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImageMetadata } from './ImageMetadata.ts';

export type Entry_Entries = {
    key?: string;
    value?: ImageMetadata;
};

