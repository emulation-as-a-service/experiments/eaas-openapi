/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type xml_ns1_abstractDataResource = {
    dataResourceType?: string;
    id?: string;
};

