/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SmArcLink } from './SmArcLink.ts';
import type { SmLocatorLink } from './SmLocatorLink.ts';

export type SmLinkGrp = {
    smLocatorLink?: Array<SmLocatorLink>;
    smArcLink?: Array<SmArcLink>;
    ID?: string;
    role?: string;
    type?: string;
    title?: string;
    ARCLINKORDER?: string;
};

