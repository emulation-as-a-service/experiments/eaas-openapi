/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_networkingType } from './xml_ns0_networkingType.ts';

export type xml_ns0_containerNetworkingType = (xml_ns0_networkingType & {
    isDHCPenabled?: boolean;
    isTelnetEnabled?: boolean;
});

