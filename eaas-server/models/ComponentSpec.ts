/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ComponentSpec = {
    componentId?: string;
    networkLabel?: string;
    serverPorts?: Array<number>;
    serverIp?: string;
    fqdn?: string;
    hwAddress?: string;
};

