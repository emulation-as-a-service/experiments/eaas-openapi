/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AreaType } from './AreaType.ts';
import type { SeqType } from './SeqType.ts';

export type ParType = {
    area?: Array<AreaType>;
    seq?: Array<SeqType>;
    ID?: string;
};

