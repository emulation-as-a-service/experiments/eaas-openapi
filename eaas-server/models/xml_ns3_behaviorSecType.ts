/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns3_behaviorType } from './xml_ns3_behaviorType.ts';

export type xml_ns3_behaviorSecType = {
    ID?: string;
    CREATED?: string;
    LABEL?: string;
    behaviorSec?: Array<xml_ns3_behaviorSecType>;
    behavior?: Array<xml_ns3_behaviorType>;
};

