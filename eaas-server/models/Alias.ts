/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Alias = {
    name?: string;
    version?: string;
    alias?: string;
};

