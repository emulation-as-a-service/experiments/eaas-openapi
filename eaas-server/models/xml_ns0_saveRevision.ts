/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_snapshotRequest } from './xml_ns0_snapshotRequest.ts';

export type xml_ns0_saveRevision = (xml_ns0_snapshotRequest & {
    softwareId?: string;
});

