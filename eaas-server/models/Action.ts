/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum Action {
    COPY = 'copy',
    EXTRACT = 'extract',
    RSYNC = 'rsync',
}
