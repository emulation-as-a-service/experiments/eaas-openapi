/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { JaxbType } from './JaxbType.ts';

export type SoftwareDescription = (JaxbType & {
    isPublic?: boolean;
    isOperatingSystem?: boolean;
    archiveId?: string;
    label?: string;
    softwareId?: string;
});

