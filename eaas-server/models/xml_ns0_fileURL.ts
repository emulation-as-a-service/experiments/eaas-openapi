/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns0_baseFileSource } from './xml_ns0_baseFileSource.ts';

export type xml_ns0_fileURL = (xml_ns0_baseFileSource & {
    url?: string;
});

