/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ImportFileInfo = {
    url?: string;
    deviceId?: string;
    fileFmt?: string;
    filename?: string;
};

