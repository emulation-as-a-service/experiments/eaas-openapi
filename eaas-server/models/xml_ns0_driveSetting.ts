/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { xml_ns1_drive } from './xml_ns1_drive.ts';

export type xml_ns0_driveSetting = {
    drive?: xml_ns1_drive;
    driveIndex?: number;
    imageArchive?: string;
    imageId?: string;
    objectArchive?: string;
    objectId?: string;
    operatingSystem?: string;
};

