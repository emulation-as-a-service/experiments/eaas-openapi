/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EnvironmentInfo } from './EnvironmentInfo.ts';

export type OverrideCharacterizationRequest = {
    description?: string;
    objectId?: string;
    environments?: Array<EnvironmentInfo>;
    objectArchive?: string;
};

