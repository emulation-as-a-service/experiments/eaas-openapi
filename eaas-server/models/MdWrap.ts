/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { XmlData_MdWrap } from './XmlData_MdWrap.ts';

export type MdWrap = {
    binData?: string;
    xmlData?: XmlData_MdWrap;
    ID?: string;
    SIZE?: number;
    CHECKSUMTYPE?: string;
    OTHERMDTYPE?: string;
    CREATED?: number;
    MDTYPE?: string;
    LABEL?: string;
    MIMETYPE?: string;
    CHECKSUM?: string;
    MDTYPEVERSION?: string;
};

