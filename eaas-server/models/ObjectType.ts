/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ObjectType = {
    href?: string;
    ID?: string;
    LOCTYPE?: string;
    show?: string;
    type?: string;
    OTHERLOCTYPE?: string;
    arcrole?: string;
    role?: string;
    LABEL?: string;
    title?: string;
    actuate?: string;
};

