/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type RuntimeListItem = {
    id?: string;
    name?: string;
    description?: string;
};

