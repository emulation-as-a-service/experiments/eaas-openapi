/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type NativeConfig = {
    value?: string;
    linebreak?: string;
};

