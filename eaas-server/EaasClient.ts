/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BaseHttpRequest } from './core/BaseHttpRequest.ts';
import type { OpenAPIConfig } from './core/OpenAPI.ts';
import { FetchHttpRequest } from './core/FetchHttpRequest.ts';

import { AdminService } from './services/AdminService.ts';
import { ComponentsService } from './services/ComponentsService.ts';
import { ComputeService } from './services/ComputeService.ts';
import { EmilService } from './services/EmilService.ts';
import { EmilContainerDataService } from './services/EmilContainerDataService.ts';
import { EmilEnvironmentDataService } from './services/EmilEnvironmentDataService.ts';
import { EmilObjectDataService } from './services/EmilObjectDataService.ts';
import { EmilSoftwareDataService } from './services/EmilSoftwareDataService.ts';
import { EmilUserSessionService } from './services/EmilUserSessionService.ts';
import { EmulatorRepositoryService } from './services/EmulatorRepositoryService.ts';
import { EnvironmentProposerService } from './services/EnvironmentProposerService.ts';
import { EnvironmentRepositoryService } from './services/EnvironmentRepositoryService.ts';
import { ErrorReportService } from './services/ErrorReportService.ts';
import { HandlesService } from './services/HandlesService.ts';
import { MetaDataRepositoriesService } from './services/MetaDataRepositoriesService.ts';
import { NetworkEnvironmentsService } from './services/NetworkEnvironmentsService.ts';
import { NetworksService } from './services/NetworksService.ts';
import { ObjectClassificationService } from './services/ObjectClassificationService.ts';
import { ObjectRepositoryService } from './services/ObjectRepositoryService.ts';
import { OperatorService } from './services/OperatorService.ts';
import { SessionsService } from './services/SessionsService.ts';
import { SoftwareRepositoryService } from './services/SoftwareRepositoryService.ts';
import { TaskManagerService } from './services/TaskManagerService.ts';
import { UploadService } from './services/UploadService.ts';

type HttpRequestConstructor = new (config: OpenAPIConfig) => BaseHttpRequest;

export class EaasClient {

    public readonly admin: AdminService;
    public readonly components: ComponentsService;
    public readonly compute: ComputeService;
    public readonly emil: EmilService;
    public readonly emilContainerData: EmilContainerDataService;
    public readonly emilEnvironmentData: EmilEnvironmentDataService;
    public readonly emilObjectData: EmilObjectDataService;
    public readonly emilSoftwareData: EmilSoftwareDataService;
    public readonly emilUserSession: EmilUserSessionService;
    public readonly emulatorRepository: EmulatorRepositoryService;
    public readonly environmentProposer: EnvironmentProposerService;
    public readonly environmentRepository: EnvironmentRepositoryService;
    public readonly errorReport: ErrorReportService;
    public readonly handles: HandlesService;
    public readonly metaDataRepositories: MetaDataRepositoriesService;
    public readonly networkEnvironments: NetworkEnvironmentsService;
    public readonly networks: NetworksService;
    public readonly objectClassification: ObjectClassificationService;
    public readonly objectRepository: ObjectRepositoryService;
    public readonly operator: OperatorService;
    public readonly sessions: SessionsService;
    public readonly softwareRepository: SoftwareRepositoryService;
    public readonly taskManager: TaskManagerService;
    public readonly upload: UploadService;

    public readonly request: BaseHttpRequest;

    constructor(config?: Partial<OpenAPIConfig>, HttpRequest: HttpRequestConstructor = FetchHttpRequest) {
        this.request = new HttpRequest({
            BASE: config?.BASE ?? '',
            VERSION: config?.VERSION ?? '0.0.1-SNAPSHOT',
            WITH_CREDENTIALS: config?.WITH_CREDENTIALS ?? false,
            CREDENTIALS: config?.CREDENTIALS ?? 'include',
            TOKEN: config?.TOKEN,
            USERNAME: config?.USERNAME,
            PASSWORD: config?.PASSWORD,
            HEADERS: config?.HEADERS,
            ENCODE_PATH: config?.ENCODE_PATH,
        });

        this.admin = new AdminService(this.request);
        this.components = new ComponentsService(this.request);
        this.compute = new ComputeService(this.request);
        this.emil = new EmilService(this.request);
        this.emilContainerData = new EmilContainerDataService(this.request);
        this.emilEnvironmentData = new EmilEnvironmentDataService(this.request);
        this.emilObjectData = new EmilObjectDataService(this.request);
        this.emilSoftwareData = new EmilSoftwareDataService(this.request);
        this.emilUserSession = new EmilUserSessionService(this.request);
        this.emulatorRepository = new EmulatorRepositoryService(this.request);
        this.environmentProposer = new EnvironmentProposerService(this.request);
        this.environmentRepository = new EnvironmentRepositoryService(this.request);
        this.errorReport = new ErrorReportService(this.request);
        this.handles = new HandlesService(this.request);
        this.metaDataRepositories = new MetaDataRepositoriesService(this.request);
        this.networkEnvironments = new NetworkEnvironmentsService(this.request);
        this.networks = new NetworksService(this.request);
        this.objectClassification = new ObjectClassificationService(this.request);
        this.objectRepository = new ObjectRepositoryService(this.request);
        this.operator = new OperatorService(this.request);
        this.sessions = new SessionsService(this.request);
        this.softwareRepository = new SoftwareRepositoryService(this.request);
        this.taskManager = new TaskManagerService(this.request);
        this.upload = new UploadService(this.request);
    }
}

