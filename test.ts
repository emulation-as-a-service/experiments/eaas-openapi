import "./debug.ts";

import { EaasClient } from "./eaas-server/EaasClient.ts";
import type {
  EmilEnvironment,
  ImageDataSource,
  ImageMetaData,
  MachineComponentRequest,
} from "./eaas-server/index.ts";

const client = new EaasClient({
  BASE:
    "https://6ee231d1-4776-4d97-b54c-a1afb2eb2adc.fr.bw-cloud-instance.org/emil",
});

const environments = await client.environmentRepository
  .migrateDb9() as any as Array<EmilEnvironment>;

const images = await client.environmentRepository.migrateDb16() as any as Array<
  ImageMetaData
>;

const enviornment = environments[0];

const { id } = await client.components.createComponent({
  environment: enviornment.envId,
  drives: [{
    id: "1",
    bootable: true,
    data: {
      kind: "image",
      id: images[0].id,
    } as ImageDataSource,
  }],
} as MachineComponentRequest);

console.log(id);
