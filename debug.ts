const logFn = (name, ...params) => {
    console.debug(`${name}(${JSON.stringify(params).slice(1, -1)})`);
  }
  
  globalThis.fetch = new Proxy(globalThis.fetch, {
    apply(target, thisArg, argArray) {
        const [url, request] = argArray;
        logFn("fetch", url, {
            method: request.method,
            headers: Object.fromEntries(request.headers),
            body: request.body,
        });
        return Reflect.apply(target, thisArg, argArray);
    }
  });